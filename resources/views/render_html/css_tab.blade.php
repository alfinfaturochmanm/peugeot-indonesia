<style type="text/css">
    .css-editor-container {
        position: fixed;
        bottom: 0px;
        left: 0px;
        width: 100%;
        height: 300px;
        z-index: 10;
        background-color: #fff;
        box-shadow: 0px 2px 23px #3a3a3a;
    }
    .css-editor-container h5{
        border-bottom: 1px #4CAF50 solid;
        width: max-content;
        font-size: 20px;
        margin: 0px;
    }
    .css-editor-container .responsive_resize {
        display: table; padding-left: 0px; list-style: none; font-size: 12px; width: 100%; margin-bottom: 0px; padding: 5px; 
    }
    .css-editor-container .responsive_resize li {
        float:left; width: 100%; display: none;
    }
    .css-editor-container .responsive_resize li textarea{
        width: 100%;
        height: 200px;
        padding: 10px;
        font-family: 'consolas';
        font-size: 16px;
        border: 1px #4CAF50 solid;
        line-height: 1.1;
    }
    .css-editor-container .responsive_resize .active {
        display: block;
    }
    .btn-container-width {
        display: table;
        width: 100%;
    }
    .btn-container-width .btn{
        width: 48%;
        float: left;
        color: #fff;
        border-radius: 0px;
        font-weight: bold;
        font-size: 20px;
        background-color: #282a2c;
        text-decoration: none;
        text-align: center;
        padding: 17px 0px !important;
        border: none;
        margin-right: 1%;
        text-transform: uppercase;
    }

</style>

<div class="css-editor-container">
    <form method="post" id="form_css_tab">
        <ul class="responsive_resize">
        @php
            $medias = explode('@media', $intepreter['head']['css']['main']);
            foreach($medias as $key => $media) {
                if($key > 0) {
                    $size = str_replace('only screen and ', '', explode('{', $media)[0]);
                    $size_xpld = explode('width: ', $size);
                    $maxWidth = explode('px) and (min-', $size_xpld[1])[0];
                    $minWidth = explode('px)', $size_xpld[2])[0];
        @endphp

                <li max-width="{{$maxWidth}}" min-width="{{$minWidth}}">
                    <h5>Resolution : {{$maxWidth}} px - {{$minWidth}} px</h5>
                    <textarea max-width="{{$maxWidth}}" min-width="{{$minWidth}} " class="_textarea_css" name="css_'{{$maxWidth}}_{{$minWidth}}"><?php echo str_replace('}}', '}', explode(')  {', $media)[1]); ?></textarea>
                </li>

        @php
                } else {
        @endphp

                <li max-width="1900" min-width="1201">
                    <h5>Resolution : Desktop (1900px - 1201px)</h5>
                    <textarea max-width="1900" min-width="1201" class="_textarea_css" name="css_1900_1201">{{$media}}</textarea>
                </li>
                
        @php
                }
            }
        @endphp
        </ul>
        <div class="btn-container-width">
            <a class="btn btn-warning" href="javascript:void(0)" id="btn_css_update">Update<a/>
            <input type="submit" class="btn btn-primary" value="Save" style="width: 50%; float: left" />
        </div>
        </form>
</div>

<script type="text/javascript">
    function responsive_resize() {
        size = $(window).width();
        var lit = $('.responsive_resize li').filter(function(){
            return $(this).attr('max-width') >= size && $(this).attr('min-width') <= size;
        });
        $('.responsive_resize li').removeClass('active');
        lit.addClass('active');
    }
    $(window).on('resize', function(){
        responsive_resize();
    })
    $('#btn_css_update').click(function(){
        var _textarea_css = $('._textarea_css');
        var css = '';
        for(var i = 0; i < _textarea_css.length; i++) {
            if(i > 0)
                css += '@media only screen and (max-width: ' + _textarea_css.eq(i).attr('max-width') + 'px) and (min-width: ' + _textarea_css.eq(i).attr('min-width') + 'px)  {';
            css += _textarea_css.eq(i).val();
            if(i == 0)
                console.log(_textarea_css.eq(i).attr('name') + ' ' + _textarea_css.eq(i).val());
            if(i > 0)
                css += '}';
        }
        $('.style_text_css').html(css);
    });
    responsive_resize();
    $("#form_css_tab").submit(function(e){
        e.preventDefault();
        var css_1900_1201 = $('textarea[name=css_1900_1201]').val();
        console.log(css_1900_1201);
        $.ajax({
            url: '{{ url("api/css/update") }}',
            type: "post",
            data: {
                css_1900_1201: css_1900_1201
            }
        });
    });
</script>