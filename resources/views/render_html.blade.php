<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {!! $intepreter['head']['meta'] !!}
        <title>{!! $intepreter['head']['title'] !!} </title>
        <link href="{{ url('css/slider-10.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/custom.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/styleguide.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/navbar.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/slider-5.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/slider-6.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/slider-7.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/slider-9.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/slider-11.css') }}" rel="stylesheet"/>
    </head>
    <body class="{{ $intepreter['body']['class'] }}" url="{{url('/home')}}">

        <style type="text/css" class="style_text_css">

            {!! $intepreter['head']['css']['main'] !!}
        
        </style>
        <style type="text/css">

            {!! $intepreter['head']['css']['library_component'] !!}

        </style>

        {!! $intepreter['body']['navbar'] !!}

        {!! $intepreter['body']['section'] !!}

        {!! $intepreter['body']['footer'] !!}
        
    </body>

    {!! $intepreter['footer']['javascript'] !!}

    <script type="text/javascript" src="{{ url('js/slider-10.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/main.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/navbar.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/slider-5.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/slider-6.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/slider-7.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/slider-9.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/slider-11.js') }}"></script>
    <script type="text/javascript">
        $(document).scroll(function(){
            var sct = $(document).scrollTop(),
                wh = $(window).height();
            if(sct > wh) {
                if(!$('._navbar').hasClass('_fill-inline'))
                    $('._navbar').addClass('_fill-inline');
            } else {
                if($('._navbar').hasClass('_fill-inline'))
                    $('._navbar').removeClass('_fill-inline');
            }
        })
    </script>

    @if(isset($_GET['css_tab']))
    
        @include('render_html.css_tab', $intepreter)
    
    @endif

</html>