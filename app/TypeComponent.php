<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeComponent extends Model
{
    
    protected $table = 'type_component';

    public function d_css_style_guide() {
        return $this->hasMany('App\CssStyleGuide', 'type_component', 'id');
    }

}
