<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grids extends Model
{

    protected $fillable = ['name', 'html_id', 'html_class', 'reference_id', 'length', 'sequence', 'sections'];

    public function m_sections() {
        return $this->belongsTo('App\Sections', 'sections');
    }

    public function d_components() {
        if($this->reference_id != '')
            return $this->hasMany('App\Components', 'grids', 'reference_id')
                ->where('status', 1);
        else
            return $this->hasMany('App\Components', 'grids')
                ->where('status', 1);
    }
    
}
