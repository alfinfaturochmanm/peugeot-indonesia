var cardNews = $('._section-event-promotion ._container ._grid ._card');
var lastestNews = $('._section-event-promotion ._container .grid-6:nth-child(2) p');
var olderNews = $('._section-event-promotion ._container .grid-6:nth-child(3) p');

lastestNews.addClass('active');
showNews();

lastestNews.click(function() {
    if (lastestNews.attr('class') == 'active' || olderNews.attr('class') == 'active'){
        lastestNews.removeClass('active');
        olderNews.removeClass('active');
    }
    lastestNews.addClass('active');
    showNews();
});

olderNews.click(function() {
    if (lastestNews.attr('class') == 'active' || olderNews.attr('class') == 'active'){
        lastestNews.removeClass('active');
        olderNews.removeClass('active');
    }
    olderNews.addClass('active');
    showNews();
});

function showNews() {
    if(lastestNews.attr('class') == 'active'){
        if (cardNews.length >= 4){
            cardNews.css('display','none');
            cardNews.removeClass('active');
            var i;
            for(i = 0 ; i < 4 ; i++){
                cardNews[i].style.display = "block";
                cardNews[i].classList.add('active');
            }
        }
    }else{
        if (cardNews.length >= 4){
            cardNews.css('display','none');
            cardNews.removeClass('active');
            var i;
            for(i = 4 ; i < 8 ; i++){
                cardNews[i].style.display = "block";
                cardNews[i].classList.add('active');
            }
        }
    }
};

if($('._page_product_new-5008').length > 0 || $('._page_product_new-3008').length > 0) {
    var a = parseInt($(window).height())/3,
        b = $('._section-indicator-pointer');
    c = '';
    if(b.length > 0) {
        c += '<div class="_section-indicator">'
            + '<ul>';

        $.each(b, function(k, v){
            c += '<li></li>';
        })

        c += '</ul>'
            + '</div>';

        $('body').append(c);

        h = $('._section-indicator').height();
        console.log(h);
        $('._section-indicator').css('margin-top', -(parseInt(h) / 2));
        g = $('._section-indicator ul li');
        function resetScroll() {
            b = $('._section-indicator-pointer');
            d = Math.round($(document).scrollTop())
            for(k = 0; k < b.length; k++) {
                e = parseInt(b.eq(k).offset().top);
                if(b.eq(k + 1).length > 0)
                    f = parseInt(b.eq(k + 1).offset().top);
                else
                    f = parseInt(b.eq(k).offset().top) + parseInt(b.eq(k).height());
                if(d >= (e - a) && d < (f - a)) {
                    console.log((e - a) + ' <= ' + d + ' < ' +  (f - a));
                    g.removeClass('active');
                    g.eq(k).addClass('active');
                }
            }
        }
        $(document).on('scroll', function() { resetScroll(); })
        resetScroll();

    }
}