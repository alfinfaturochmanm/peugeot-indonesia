$('.section_preview_nav').click(function() {
    $('.section_preview_nav').removeClass('active');
    var idx = $(this).index();
    $(this).addClass('active');
    $('#sliderProductPreview .slider-item').removeClass('active');
    $('#sliderProductPreview .slider-item').eq(idx).addClass('active');
    initSlider6();
})
$('.section_preview_slider_tab').click(function() {
    if (!$(this).hasClass('active')) {
        $('.section_preview_slider_tab').removeClass('active');
        var idx = $(this).index();
        $(this).addClass('active');
        var sp = $('._section-preview-product .slider-preview');
        sp.removeClass('tab-active');
        sp.eq(idx).addClass('tab-active');
    }
})

function iasClick(ias_id, idx) {
    if (idx == 'prev' || idx == 'next') {
        var is = $(ias_id + ' ._item-additional-slider'),
            x = '';
        for (var i = 0; i < is.length; i++)
            if (is.eq(i).hasClass('active'))
                x = i;
        if (idx == 'prev')
            idx = x - 1;
        if (idx == 'next')
            idx = x + 1;
        if (is.length == idx)
            idx = 0;
    }

    var cs = $('._content-slider');
    for (var i = 0; i < cs.length; i++) {
        var li = cs.eq(i).find('._item-additional-slider-indicator li');
        li.removeClass('active');
        li.eq(idx).addClass('active');
        var is = cs.eq(i).find('._item-additional-slider');
        is.removeClass('active');
        is.eq(idx).addClass('active');
        is = cs.eq(i).find('._item-additional-slider');
        for (var j = 0; j < is.length; j++) {
            if (j < idx)
                is.eq(j).css('left', -($(window).width()));
            else
                is.eq(j).css('left', ($(window).width()));
            is.eq(j).css('margin-left', 0);
        }
    }
}

function initSlider6() {
    var indicator = $('#sliderProductPreview .active ._additional-slider ._indicator-additional-slider li');
    var cs = $('#sliderProductPreview .active ._additional-slider ._content-additional-slider ._content-slider');
    for (var i = 0; i < cs.length; i++) {
        var ias_id = '#' + cs.eq(i).attr('id');
        var ias = $(ias_id).find('._item-additional-slider');
        if (ias.length > 0) {
            var idct = '<ul class="_item-additional-slider-indicator" style="margin-left: -' + (35 * ias.length) / 2 + 'px">';
            for (var j = 0; j < ias.length; j++) {
                var img = ias.eq(j).find('img'),
                    text = ias.eq(j).find('h5');
                if (j > 0) {
                    ias.eq(j).css('left', ($(window).width()));
                    ias.eq(j).css('margin-left', 0);
                }
                idct += '<li class="' + (j == 0 ? 'active' : '') + '" onclick="iasClick(\'' + ias_id + '\', ' + j + ')"></li>';
            }
            idct += '</ul>';
            if($('#sliderProductPreview .active ._additional-slider ._content-additional-slider ._content-slider ul').length <= 2){
                cs.eq(i).append(idct);
                cs.eq(i).append('<div class="_slider-additional-navigation">' +
                    '<a href="javascript:void(0)" onclick="iasClick(\'' + ias_id + '\', \'prev\')" class="_btn-prev"><</a>' +
                    '<a href="javascript:void(0)" onclick="iasClick(\'' + ias_id + '\', \'next\')" class="_btn-next">></a>' +
                    '</div>'
                );
            }
        }
    }

    function setProductPreviewSlide(a) {
        var indicator = $('#sliderProductPreview .active ._additional-slider ._indicator-additional-slider li');
        indicator.removeClass('active');
        indicator.eq(a).addClass('active');
        var csl = $('#sliderProductPreview .active ._additional-slider ._content-additional-slider ._content-slider');
        csl.removeClass('active');
        csl.eq(a).addClass('active');
        var text = indicator.eq(a).text();
        $('#sliderProductPreview .active .slider-text-container .slider-description:nth-child(3)').text(text);
    }
    setProductPreviewSlide(0);
    indicator.click(function() {
        var idx = $(this).index();
        setProductPreviewSlide(idx);
    });
}
initSlider6();