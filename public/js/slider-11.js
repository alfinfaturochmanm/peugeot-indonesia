makeStructure();
pushData();

function makeStructure(){
    $('.slider-11 .slider-item-container').before('<div class="slider-indicator">');
    $('.slider-11 .slider-item-container').before('<div class="slider-preview">');
    var heading = $('.slider-11 .slider-item-container .slider-item .slider-text-container .slider-heading').length;
    $('.slider-11 .slider-indicator').append('<div class="indicator-list"></div>');
    for(i = 0 ; i < heading ; i++){
        $('.slider-11 .slider-indicator .indicator-list').append('<li><span class="fa fa-circle"></span></li>');
    }
    $('.slider-11 .slider-indicator li').eq(0).addClass('active');
}

function pushData(){
    var activeNow = $('.slider-11 .slider-item-container .active').html();
    $('.slider-11 .slider-preview').empty();
    $('.slider-11 .slider-preview').append(activeNow);
}

function indicatorToogle(){
    var itemActive = $('.slider-11 .slider-item-container .active');
    var indicatorItem = $('.slider-11 .slider-indicator li');
    var indicatorActive = $('.slider-11 .slider-indicator .active');

    indicatorActive.removeClass('active');
    indicatorItem.eq(itemActive.index()).addClass('active');
}

$('.slider-11 .slider-indicator li').click(function(){
    var itemClick = $(this).index();
    var item = $('.slider-11 .slider-item-container .slider-item');
    var active = $('.slider-11 .slider-item-container .active');

    $('.slider-11 .slider-indicator .active').removeClass('active');
    active.removeClass('active');
    $(this).addClass('active');
    item.eq(itemClick).addClass('active');
    pushData();
});

setInterval(function(){ 
    var active = $('.slider-11 .slider-item-container .active');
    var item = $('.slider-11 .slider-item-container .slider-item');
    
    active.removeClass('active');
    if(active.index()+1 < item.length){
        item.eq(active.index()+1).addClass('active');
    }else{
        item.eq(0).addClass('active');
    }

    pushData();
    indicatorToogle();

}, 5000);