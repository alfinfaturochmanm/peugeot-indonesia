    $(document).ready(function() {
        itemActive = $('.slider-5 .slider-item-container .active');
        itemIndexActive = itemActive.index();
        $(item.eq(itemIndexActive - 1).addClass('before'));
        $(item.eq(itemIndexActive + 1).addClass('after'));
        addCaption();
    });
    var itemCaption = "";
    var itemActive = "";
    var itemBefore = "";
    var itemAfter = "";
    var itemIndexActive = "";
    var itemIndexBefore = "";
    var itemIndexAfter = "";
    var textSlider = "";
    var praragraphSlider = "";
    var buttonSlider = "";
    var parent = $('.slider-5');
    var item = $('.slider-5 .slider-item-container .slider-item');
    var indicatorLength = "";
    for (i = 0; i < item.length; i++) {
        indicatorLength += "<li><span class='fa fa-circle'></span></li>";
    }

    var indicator = '<div class="slider-indicator"><div class="slider-control-prev"><span class="fa fa-chevron-up"></span></div><ul>' + indicatorLength + '</ul><div class="slider-control-next"><span class="fa fa-chevron-down"></span></div></div>';
    parent.append(indicator);
    if ($('.slider-5 .slider-item-container .active')) {
        $('.slider-5 .slider-indicator ul li').eq(itemIndexActive).addClass('active');
        $('.slider-5 .slider-indicator ul li').append("<div class='.span-line'></div>");
    }

    function addCaption() {
        textSlider = $('.slider-5 .slider-item-container').html();
        var caption = '<div class="slider-caption">' + textSlider + '</div>';
        parent.append(caption);
        $('.slider-5 .slider-caption .slider-item img').remove();
        $('.slider-5 .slider-caption .before').removeClass('before');
        $('.slider-5 .slider-caption .after').removeClass('after');
    }

    function addClass() {
        var itemCaption = $('.slider-5 .slider-caption .slider-item');
        if (itemIndexAfter == 3) {
            $(item.eq(itemIndexActive).addClass('before'));
            $(item.eq(0).addClass('after'));
            $(item.eq(itemIndexAfter).addClass('active'));
            itemCaption.eq(3).addClass('active');
        } else {
            $(item.eq(itemIndexActive).addClass('before'));
            $(item.eq(itemIndexAfter + 1).addClass('after'));
            $(item.eq(itemIndexAfter).addClass('active'));
            itemCaption.eq(itemIndexAfter).addClass('active');
        }
    }

    function addClassBefore() {
        var itemCaption = $('.slider-5 .slider-caption .slider-item');
        if (itemIndexBefore == -1) {
            $(item.eq(3).addClass('before'));
            $(item.eq(itemIndexActive).addClass('after'));
            $(item.eq(itemIndexBefore).addClass('active'));
            itemCaption.eq(itemIndex).addClass('active');
        } else {
            $(item.eq(itemIndexBefore - 1).addClass('before'));
            $(item.eq(itemIndexActive).addClass('after'));
            $(item.eq(itemIndexBefore).addClass('active'));
            itemCaption.eq(itemIndexBefore).addClass('active');
        }
    }

    function removeClass() {
        itemActive.removeClass('active');
        itemBefore.removeClass('before');
        itemAfter.removeClass('after');
        $('.slider-5 .slider-caption .active').removeClass('active');
    }

    $('.slider-control-prev').click(function() {
        itemActive = $('.slider-5 .slider-item-container .active');
        itemBefore = $('.slider-5 .slider-item-container .before');
        itemAfter = $('.slider-5 .slider-item-container .after');
        itemIndexActive = itemActive.index();
        itemIndexBefore = itemBefore.index();
        itemIndexAfter = itemAfter.index();
        if (item.eq(itemIndexAfter + 1) == 3) {
            $(item.eq(3).addClass('before'));
            $(item.eq(1).addClass('after'));
            $(item.eq(0).addClass('active'));
        } else {
            removeClass();
            addClassBefore();
        }
        var liActive = $('.slider-5 .slider-indicator ul li.active');
        var li = $('.slider-5 .slider-indicator ul li');
        liActive.removeClass('active');
        li.eq(itemIndexBefore).addClass('active');
    });

    $('.slider-control-next').click(function() {
        itemActive = $('.slider-5 .slider-item-container .active');
        itemBefore = $('.slider-5 .slider-item-container .before');
        itemAfter = $('.slider-5 .slider-item-container .after');
        itemIndexActive = itemActive.index();
        itemIndexBefore = itemBefore.index();
        itemIndexAfter = itemAfter.index();
        if (item.eq(itemIndexAfter + 1) == 3) {
            $(item.eq(3).addClass('before'));
            $(item.eq(1).addClass('after'));
            $(item.eq(0).addClass('active'));
        } else {
            removeClass();
            addClass()
        }
        var liActive = $('.slider-5 .slider-indicator ul li.active');
        var li = $('.slider-5 .slider-indicator ul li');
        liActive.removeClass('active');
        li.eq(itemIndexAfter).addClass('active');
    });

    $('.slider-5 .slider-indicator ul li').click(function() {
        itemCaption = $('.slider-5 .slider-caption .slider-item');
        itemActive = $('.slider-5 .slider-item-container .active');
        itemBefore = $('.slider-5 .slider-item-container .before');
        itemAfter = $('.slider-5 .slider-item-container .after');
        removeClass();
        $(item.eq($(this).index()).addClass('active'));
        itemActive = $('.slider-5 .slider-item-container .active');
        itemIndexActive = itemActive.index();
        itemCaption.eq($(this).index()).addClass('active');
        $(item.eq(itemIndexActive - 1).addClass('before'));
        if ($(this).index() == 3) {
            $(item.eq(0).addClass('after'));
        } else {
            $(item.eq(itemIndexActive + 1).addClass('after'));
        }
        var liActive = $('.slider-5 .slider-indicator ul li.active');
        var li = $('.slider-5 .slider-indicator ul li');
        liActive.removeClass('active');
        li.eq($(this).index()).addClass('active');
    });