makeStructure();
setActive();
$('._section-navbar ._container .navbar-expand').css('display','none');

$(document).scroll(function(){
    var sct = $(document).scrollTop(),
        wh = $(window).height();
    if(sct > wh) {
        if(!$('._section-navbar ._container').hasClass('_fill-inline'))
            $('._section-navbar ._container').addClass('_fill-inline');
    } else {
        if($('._section-navbar ._container').hasClass('_fill-inline'))
            $('._section-navbar ._container').removeClass('_fill-inline');
    }
})


$('._section-navbar ._container ._navbar-object ._site-icon-list').click(function(){
    if($('._section-navbar ._container .navbar-expand').css('display') == "none"){
        $('._section-navbar ._container .navbar-expand').css('display','block');
    }else{
        $('._section-navbar ._container .navbar-expand').css('display','none');
    }
})

$('._section-navbar ._container .navbar-expand ._main-list ._nav-list').click(function(){
    var main = $('._section-navbar ._container .navbar-expand ._main-list .active');
    main.removeClass('active')
    var click = $(this).index();
    $('._section-navbar ._container .navbar-expand ._main-list ._nav-list').eq(click).addClass('active');
    setActive();
})

$('._section-navbar ._container .navbar-expand ._child-list ._sub-group ._sub-list').hover(function(){
    var main = $('._section-navbar ._container .navbar-expand ._child-list ._sub-group .active');
    main.removeClass('active fas fa-caret-right')
    var click = $(this).index();
    $('._section-navbar ._container .navbar-expand ._child-list ._sub-group ._sub-list').eq(click).addClass('active fas fa-caret-right');
    
    var previewChild = $('._section-navbar ._container .navbar-expand ._preview-list ._prev-group');
    var ActiveChild = $('._section-navbar ._container .navbar-expand ._preview-list .active');
    ActiveChild.find('._prev-sub-group').removeClass('active');
    previewChild.find('._prev-sub-group').eq(click).addClass('active');
})

$('._section-navbar ._container .navbar-expand ._child-list ._sub-group ._sub-list').click(function(){
    if($(this).index() == 0){
        location.href="/product/new-3008";
    }else if($(this).index() == 1){
        location.href="/product/new-5008";
    }
})

function makeStructure() {
    var navbar = $('._section-navbar ._container');
    var navbarExpand = $('._section-navbar ._container ._navbar-expand');
    navbar.append('<div class="navbar-expand">');
    navbarExpand.appendTo('._section-navbar ._container .navbar-expand');
    navbarExpand+$('._navbar-expand').removeClass('_navbar-expand');
    
    var subList = $('.navbar-expand ._child-list');
    var prevList = $('.navbar-expand ._preview-list');
    var length = subList.children().length;

    for(i = 1 ; i <= length ; i++){

        if(subList.children().hasClass('_item-'+i)){

            subList.append('<div class="_sub-group _sub-list-item-'+i+'"></div>');
            $('.navbar-expand ._child-list ._item-'+i).appendTo($('.navbar-expand ._child-list ._sub-list-item-'+i));

            if(prevList.children().hasClass('_item-'+i)){

                prevList.append('<div class="_prev-group _prev-list-item-'+i+'"></div>');
                $('.navbar-expand ._preview-list ._item-'+i).appendTo($('.navbar-expand ._preview-list ._prev-list-item-'+i));
                var childPrevList = $('.navbar-expand ._preview-list ._prev-list-item-'+i);
                var childPrevListLength = childPrevList.children().length;

                for(b = 1 ; b <= childPrevListLength/2 ; b++){
                    
                    var cok = b;
                    if(childPrevList.children().hasClass('_sub-'+b)){

                        childPrevList.append('<div class="_prev-sub-group _prev-list-sub-'+b+'"></div>');
                        $('.navbar-expand ._preview-list ._prev-list-item-'+i+' ._sub-'+cok).appendTo($('.navbar-expand ._preview-list ._prev-list-item-'+i+' ._prev-list-sub-'+b));
    
                    }
                }
            }
        }
    }
}

function setActive(){
    var main = $('._section-navbar ._container .navbar-expand ._main-list');
    var subParent = $('._section-navbar ._container .navbar-expand ._child-list .active');
    var subChild = $('._section-navbar ._container .navbar-expand ._child-list ._sub-group .active');
    var preview = $('._section-navbar ._container .navbar-expand ._preview-list .active');
    var previewChild = $('._section-navbar ._container .navbar-expand ._preview-list ._prev-group .active');
    var pathname = 'aftersales';
    $data = [];
    for(i = 0 ; i < main.find('._nav-list').length ; i++){
        $data.push(main.find('._nav-list')[i].innerHTML);
    }
    var counter = 0;
    if(main.find('.active').length == 0){
        $data.forEach(item => {
            counter++
            console.log(item);
            if(pathname.match(item)){
                console.log(counter);
            }
        });
        main.find('._nav-list').eq(0).addClass('active');
        setSubActive();
    }else{
        subParent.removeClass('active');
        subChild.removeClass('active fas fa-caret-right');
        preview.removeClass('active');
        previewChild.removeClass('active');
        setSubActive();
    }
}

function setSubActive(){
    var main = $('._section-navbar ._container .navbar-expand ._main-list');
    var subParent = $('._section-navbar ._container .navbar-expand ._child-list');
    var subChild = $('._section-navbar ._container .navbar-expand ._child-list ._sub-group');
    var preview = $('._section-navbar ._container .navbar-expand ._preview-list');
    var previewChild = $('._section-navbar ._container .navbar-expand ._preview-list ._prev-group');
    var eqMain = main.find('.active').index();
    subParent.find('._sub-group').eq(eqMain).addClass('active');
    subChild.find('._sub-list').eq(eqMain).addClass('active fas fa-caret-right');
    preview.find('._prev-group').eq(eqMain).addClass('active');
    previewChild.find('._prev-sub-group').eq(eqMain).addClass('active');
}

$('._navbar-object ._site-logo:nth-child(1)').click(function(){
    window.location.href = $('body').attr('url');
})