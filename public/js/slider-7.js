function initSlider7() {
    var sni = $('#sliderProductNavigate .slider-item'),
        cw = $('#sliderProductNavigate').width(),
        col = Math.ceil(parseInt(sni.length) / 4),
        active = 1;
    html = '<div class="slider-container" style="width:' + (cw * col) + 'px">';
    for (var i = 0; i < sni.length; i++) {
        var si = sni.eq(i);
        var image = si.find('img');
        var heading = si.find('.slider-heading');
        html += '<div class="slider-item">';
        html += '<img src="' + image.attr('src') + '">';
        html += '<div class="slider-text-container">';
        html += '<h4 class="slider-heading">';
        html += heading.text();
        html += '</h4>';
        html += '</div>';
        html += '</div>';
    }
    html += '</div>';
    html += '<div class="_slider-additional-navigation">' +
        '<a href="javascript:void(0)" onclick="sliderSevenmoveTo(\'prev\')" class="_btn-prev"><</a>' +
        '<a href="javascript:void(0)" onclick="sliderSevenmoveTo(\'next\')" class="_btn-next">></a>' +
        '</div>';
    $('#sliderProductNavigate').html(html);
    sliderSevenmoveTo(1);
}

function sliderSevenmoveTo(idx) {
    var sni = $('#sliderProductNavigate .slider-item'),
        cw = $('#sliderProductNavigate').width(),
        col = Math.ceil(parseInt(sni.length) / 4);
    if (idx == 'prev')
        idx = parseInt($('#sliderProductNavigate').attr('current')) - 1;
    else if (idx == 'next')
        idx = parseInt($('#sliderProductNavigate').attr('current')) + 1;
    if (idx > col)
        idx = 1;
    else if (idx == 0)
        idx = col;
    $('#sliderProductNavigate').attr('current', idx);
    $('.slider-container').css('left', (cw - (cw * idx)));
}
initSlider7();