$('.slider-10 .slider-item-container .slider-item .slider-text-container').css("display","none");
if($('.slider-10 .type-1')){
    type1();
}else if($('.slider-10 .type-2')){
    type2();
}

function type1(){
    makeStructure();
    pushDataType10();
}

function type2(){
    console.log('halew');
}

function makeStructure(){
    $('.slider-10 .slider-item-container.type-1').before('<div class="slider-indicator type-1">');
    $('.slider-10 .slider-item-container.type-1').before('<div class="slider-preview type-1">');
    $('.slider-10 .slider-indicator.type-1').append('<div class="indicator-list"></div>');
    var heading = $('.slider-10 .slider-item-container.type-1 .slider-item .slider-text-container .slider-heading').length;
    for(i = 1 ; i <= heading ; i++){
        if(i % 3 == 0){
            var image = $('.slider-10 .slider-item-container.type-1 .slider-item').eq(i-2).find('img').addClass('video-cover');
            var embed = $('.slider-10 .slider-item-container.type-1 .slider-item').eq(i-2).find('embed').addClass('_videoPlayer');
            $('.slider-10 .slider-item-container.type-1 .slider-item .slider-text-container').eq(i-2).before('<div class="video-1"></div>');
            var select= $('.slider-10 .slider-item-container.type-1 .slider-item').eq(i-2).find('.video-1');
            var button = '<img class="video-button" src="./uploads/image/btnVideo.png">'
            image.appendTo(select);
            embed.appendTo(select);
            $('.slider-10 .slider-item-container.type-1 .slider-item').eq(i-2).find('.video-1').append(button);
        }
    }
    for(i = 3 ; i <= heading ; i++){
        if(i % 3 == 0){
            $('.slider-10 .slider-indicator.type-1 .indicator-list').append('<li><span class="fa fa-circle"></span></li>');
        }
    }
    $('.slider-10 .slider-indicator li').eq(0).addClass('active');
    $('.slider-10 .slider-item-container.type-1 .slider-item').eq(1).addClass('active');
    $('.slider-10 .slider-item-container.type-1 .slider-item').eq(2).addClass('active');
}

function pushDataType10(){
    var activeNow = $('.slider-10 .slider-item-container.type-1 .active');
    $('.slider-10 .slider-preview.type-1').empty();
    activeNow.clone().appendTo($('.slider-10 .slider-preview.type-1'));
}

$('.slider-10 .slider-preview.type-1 .slider-item .video-1 .video-button').on('click', function(e) {
    var videoPlayer = $('.slider-10 .slider-preview.type-1 .slider-item .video-1 ._videoPlayer');
    var cover = $('.slider-10 .slider-preview.type-1 .slider-item .video-1 .video-cover');
    var buttonPlay = $('.slider-10 .slider-preview.type-1 .slider-item .video-1 .video-button');
    console.log('comk');
    e.preventDefault();
    videoPlayer.src += "?autoplay=1";
    videoPlayer.show();
    cover.hide();
    buttonPlay.hide();
})

$('.slider-10 .slider-indicator.type-1 li').click(function(){
    var itemClick = $(this).index();
    var item = $('.slider-10 .slider-item-container.type-1 .slider-item');
    var active = $('.slider-10 .slider-item-container.type-1 .active');

    $('.slider-10 .slider-indicator.type-1 .active').removeClass('active');
    active.removeClass('active');
    $(this).addClass('active');
    item.eq(itemClick*3+2).addClass('active');
    item.eq(itemClick*3+1).addClass('active');
    item.eq(itemClick*3).addClass('active');
    pushDataType10();
});

// function indicatorToogleType10(){
//     var indicatorItem = $('.slider-10 .slider-indicator.type-1 li');
//     var indicatorActive = $('.slider-10 .slider-indicator.type-1 .active');
//     var counterActive = indicatorActive.index()+1;

//     indicatorActive.removeClass('active');
//     if(counterActive < indicatorItem.length){
//         indicatorItem.eq(counterActive).addClass('active');
//     }else{
//         indicatorItem.eq(0).addClass('active')
//     }
// }

// setInterval(function(){ 
//     var active = $('.slider-10 .slider-item-container.type-1 .active');
//     var item = $('.slider-10 .slider-item-container.type-1 .slider-item');
//     var counterActive = active.index()+1;
//     active.removeClass('active');
//     if(counterActive*3+3 <= item.length){
//         item.eq(counterActive*3+2).addClass('active');
//         item.eq(counterActive*3+1).addClass('active');
//         item.eq(counterActive*3).addClass('active');
//     }else{
//         item.eq(0).addClass('active');
//         item.eq(1).addClass('active');
//         item.eq(2).addClass('active');
//     }

//     pushDataType10();
//     indicatorToogleType10();

// }, 5000);