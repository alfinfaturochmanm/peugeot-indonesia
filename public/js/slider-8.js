makeStructure();
pushData();

function makeStructure(){
    $('.slider-8').append('<div class="slider-preview">');
    $('.slider-8').append('<div class="slider-indicator">');
    var heading = $('.slider-8 .slider-item-container .slider-item .slider-text-container .slider-heading');
    for(i = 0 ; i < heading.length ; i++){
        $($(heading)[i]).wrap('<li></li>');
    }
    var indicator = $('.slider-8 .slider-item-container .slider-item .slider-text-container li');
    var lenghtItem = indicator.length;
    var firstIndicator = indicator[0];
    var lastIndicator = indicator[lenghtItem-1];
    var anchor = '<div class="indicator indicator-prev"><span class="fa fa-chevron-left"></span></div><div class="indicator indicator-next"><span class="fa fa-chevron-right"></span></div>'
    $('.slider-8 .slider-indicator').append(anchor);
    $('.slider-8 .slider-indicator').append('<li>'+lastIndicator.innerHTML+'</li>');
    $('.slider-8 .slider-indicator').append(indicator);
    $('.slider-8 .slider-indicator').append('<li>'+firstIndicator.innerHTML+'</li>');
    $('.slider-8 .slider-indicator li').eq(1).addClass('active');
    $('.slider-8 .slider-indicator li').eq(2).addClass('after');
    $('.slider-8 .slider-indicator li').eq(0).addClass('before');
}

function pushData(){
    var activeNow = $('.slider-8 .slider-item-container .active').html();
    $('.slider-8 .slider-preview').empty();
    $('.slider-8 .slider-preview').append(activeNow);
}

$('.slider-8 .slider-indicator .indicator .fa-chevron-right').click(function(){ 
    var active = $('.slider-8 .slider-item-container .active');
    var eqActive = $('.slider-8 .slider-item-container .active').index();
    var item = $('.slider-8 .slider-item-container .slider-item');
    var count = $('.slider-8 .slider-item-container .slider-item').length;

    if(eqActive+1 < count){
        active.removeClass('active');
        item.eq(eqActive+1).addClass('active');
        indicatorRulesNext();
        pushData();
    }else{
        active.removeClass('active');
        item.eq(0).addClass('active');
        indicatorRulesNext();
        pushData();
    }
});

$('.slider-8 .slider-indicator .indicator .fa-chevron-left').click(function(){ 
    var active = $('.slider-8 .slider-item-container .active');
    var eqActive = $('.slider-8 .slider-item-container .active').index();
    var item = $('.slider-8 .slider-item-container .slider-item');
    var count = $('.slider-8 .slider-item-container .slider-item').length;

    if(eqActive-1 >= 0){
        active.removeClass('active');
        item.eq(eqActive-1).addClass('active');
        indicatorRulesPrev();
        pushData();
    }else{
        active.removeClass('active');
        item.eq(3).addClass('active');
        indicatorRulesPrev();
        pushData();
    }
});

$('.slider-8 .slider-indicator li').click(function(){ 
    var eqItem = $(this).index();
    var active = $('.slider-8 .slider-item-container .active');
    var indicatorActive=$('.slider-8 .slider-indicator .active');
    var item = $('.slider-8 .slider-item-container .slider-item');
    var indicator = $('.slider-8 .slider-indicator li');

    active.removeClass('active');
    indicatorActive.removeClass('active');
    item.eq(eqItem-2).addClass('active');
    indicator.eq(eqItem-1).addClass('active');
    addBeforeClass();
    addAfterClass();
    pushData();
});

function indicatorRulesNext(){
    var indicator = $('.slider-8 .slider-indicator li');
    var indicatorActive=$('.slider-8 .slider-indicator .active');
    var eqActive = $('.slider-8 .slider-item-container .active').index();
    var count = $('.slider-8  .slider-indicator li').length;
    indicatorActive.removeClass('active');

    console.log(indicatorActive.index());
    if(indicatorActive.index()+1 == count+1){
        $('.slider-8 .slider-indicator li').eq(1).addClass('active');
        addBeforeClass();
        addAfterClass();
    }else{
        if(indicatorActive.index() < count+1){
            indicator.eq(indicatorActive.index()-1).addClass('active');
            addBeforeClass();
            addAfterClass();
        }else{
            indicator.eq(0).addClass('active');
            addBeforeClass();
            addAfterClass();
        }
    }
}

function indicatorRulesPrev(){
    var indicator = $('.slider-8 .slider-indicator li');
    var indicatorActive=$('.slider-8 .slider-indicator .active');
    var eqindicatorActive=$('.slider-8 .slider-indicator .active').index();
    var count = $('.slider-8  .slider-indicator li').length;
    $('.slider-8 .slider-indicator .active').removeClass('active');

    console.log(indicatorActive.index());
    if(indicatorActive.index()-1 == 2){
        $('.slider-8 .slider-indicator li').eq(4).addClass('active');
        addBeforeClass();
        addAfterClass();
    }else{
        if(eqindicatorActive-1 >= 0){
            indicator.eq(eqindicatorActive-3).addClass('active');
            addBeforeClass();
            addAfterClass();
        }else{
            indicator.eq(3).addClass('active');
            addBeforeClass();
            addAfterClass();
        }
    }
}

function addBeforeClass(){
    var indicatorActive=$('.slider-8 .slider-indicator .active');
    var indicator = $('.slider-8 .slider-indicator li');
    $('.slider-8 .slider-indicator .before').removeClass('before');

    indicator.eq(indicatorActive.index()-3).addClass('before');
}

function addAfterClass(){
    var indicatorActive=$('.slider-8 .slider-indicator .active');
    var indicator = $('.slider-8 .slider-indicator li');
    $('.slider-8 .slider-indicator .after').removeClass('after');
    
    if(indicatorActive.index()-1 == 6){
        indicator.eq(0).addClass('after');
    }else{
        indicator.eq(indicatorActive.index()-1).addClass('after');
    }
}