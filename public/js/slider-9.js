makeStructure();
pushDataSlider9();

function makeStructure(){
    $('.slider-9 .slider-item-container').before('<div class="slider-indicator">');
    $('.slider-9 .slider-item-container').before('<div class="slider-preview">');
    var heading = $('.slider-9 .slider-item-container .slider-item .slider-text-container .slider-heading');
    var anchorPrev = '<div class="indicator indicator-prev"><span class="fa fa-chevron-left"></span></div>';
    var anchorNext = '<div class="indicator indicator-next"><span class="fa fa-chevron-right"></span></div>';
    $('.slider-9 .slider-indicator').append(anchorPrev);
    heading.clone().appendTo($('.slider-9 .slider-indicator'));
    var indicator = $('.slider-9 .slider-indicator .slider-heading');
    for(i = 0 ; i < heading.length ; i++){
        $($(indicator)[i]).wrap('<li></li>');
    }
    $('.slider-9 .slider-indicator').append(anchorNext);
    $('.slider-9 .slider-indicator li').eq(0).addClass('active');
}

function pushDataSlider9(){
    var activeNow = $('.slider-9 .slider-item-container .active').html();
    $('.slider-9 .slider-preview').empty();
    $('.slider-9 .slider-preview').append(activeNow);
}

$('.slider-9 .slider-indicator .indicator .fa-chevron-right').click(function(){ 
    var active = $('.slider-9 .slider-item-container .active');
    var eqActive = $('.slider-9 .slider-item-container .active').index();
    var item = $('.slider-9 .slider-item-container .slider-item');
    var count = $('.slider-9 .slider-item-container .slider-item').length;

    if(eqActive+1 < count){
        active.removeClass('active');
        item.eq(eqActive+1).addClass('active');
        indicatorRulesNext();
        pushDataSlider9();
    }else{
        active.removeClass('active');
        item.eq(0).addClass('active');
        indicatorRulesNext();
        pushDataSlider9();
    }
});

$('.slider-9 .slider-indicator .indicator .fa-chevron-left').click(function(){ 
    var active = $('.slider-9 .slider-item-container .active');
    var eqActive = $('.slider-9 .slider-item-container .active').index();
    var item = $('.slider-9 .slider-item-container .slider-item');
    var count = $('.slider-9 .slider-item-container .slider-item').length;

    if(eqActive-1 >= 0){
        active.removeClass('active');
        item.eq(eqActive-1).addClass('active');
        indicatorRulesPrev();
        pushDataSlider9();
    }else{
        active.removeClass('active');
        item.eq(3).addClass('active');
        indicatorRulesPrev();
        pushDataSlider9();
    }
});

$('.slider-9 .slider-indicator li').click(function(){ 
    var eqItem = $(this).index();
    var active = $('.slider-9 .slider-item-container .active');
    var indicatorActive=$('.slider-9 .slider-indicator li.active');
    var item = $('.slider-9 .slider-item-container .slider-item');
    var indicator = $('.slider-9 .slider-indicator li');

    active.removeClass('active');
    indicatorActive.removeClass('active');
    item.eq(eqItem-1).addClass('active');
    indicator.eq(eqItem-1).addClass('active');
    pushDataSlider9();
});

function indicatorRulesNext(){
    var indicator = $('.slider-9 .slider-indicator li');
    var indicatorActive=$('.slider-9 .slider-indicator li.active');
    var eqActive = $('.slider-9 .slider-item-container li.active').index();
    var count = $('.slider-9  .slider-indicator li').length;
    indicatorActive.removeClass('active');

    if(indicatorActive.index() < count){
        indicator.eq(indicatorActive.index()).addClass('active');
    }else{
        indicator.eq(0).addClass('active');
    }
}

function indicatorRulesPrev(){
    var indicator = $('.slider-9 .slider-indicator li');
    var indicatorActive=$('.slider-9 .slider-indicator li.active');
    var eqindicatorActive=$('.slider-9 .slider-indicator li.active').index();
    var count = $('.slider-9  .slider-indicator li').length;
    $('.slider-9 .slider-indicator li.active').removeClass('active');
    console.log(eqindicatorActive);

    if(eqindicatorActive-1 >= 0){
        indicator.eq(eqindicatorActive-2).addClass('active');
        console.log('true');
    }else{
        indicator.eq(3).addClass('active');
        console.log('false');
    }
}