-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.28-0ubuntu0.18.04.4 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table peugeot_indonesia.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.categories: ~0 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.components
CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `library_component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_component` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `grids` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.components: ~293 rows (approximately)
DELETE FROM `components`;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` (`id`, `html_id`, `html_class`, `content`, `library_component`, `type_component`, `sequence`, `grids`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'sliderHome', '{"0":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUV"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"1":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUS"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"2":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUR"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"3":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}}}', 'slider-5', 6, 1, 1, NULL, NULL),
	(2, NULL, NULL, '{"src":"whatsapp-badge.png"}', NULL, 4, 1, 2, NULL, NULL),
	(3, 'sliderProductPreview', 'sliderProductPreview', '{"0":{"src":"","href":"","heading":[{"tag":"h3","text":"3008 SUV"}],"description":[{"tag":"h6","text":"AVAILABLE IN"},{"tag":"h6","text":""},{"tag":"h6","text":"*OTR DKI JAKARTA"},{"tag":"h6","text":"730.000.000 IDR"}],"additional":{"slider":[{"indicator":"Copper Metalic","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Ultimate Red","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Perlescent White","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nera Black","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nimbus Grey","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_002.png","text":"*tampak belakang"}]}]}},"1":{"src":"","href":"","heading":[{"tag":"p","text":"5008 SUV"}],"description":[{"tag":"p","text":"AVAILABLE IN"},{"tag":"h6","text":""},{"tag":"p","text":"*OTR DKI JAKARTA"},{"tag":"p","text":"730.000.000 IDR"}],"additional":{"slider":[{"indicator":"Copper Metalic","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Ultimate Red","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM50NF3_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Perlescent White","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nera Black","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00N9V_0P610RFX_002.png","text":"*tampak belakang"}]},{"indicator":"Nimbus Grey","content":[{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_004.png","text":"*tampak samping"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_005.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_001.png","text":"*tampak depan"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_003.png","text":"*tampak atas"},{"image":"uploads/image/product/3008/1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_002.png","text":"*tampak belakang"}]}]}}}', 'slider-6', 6, 1, 3, NULL, NULL),
	(4, NULL, 'vidioPlayer', '{"src":"https://youtu.be/R2LQdh42neg","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', NULL, 5, 1, 8, NULL, NULL),
	(5, NULL, NULL, '{"type":"h2","value":"PRODUCT"}', NULL, 2, 1, 9, NULL, NULL),
	(6, NULL, NULL, '{"type":"h2","value":"VIDEO"}', NULL, 2, 2, 9, NULL, NULL),
	(7, NULL, NULL, '{"type":"h3","value":"FIVE YEARS PEACE OF MIND <hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 10, NULL, NULL),
	(8, NULL, NULL, '{"src":"Logo 24hr Roadside Assistance.png","href":"google.com"}', NULL, 4, 1, 11, NULL, NULL),
	(9, NULL, '_sg_line_color_2 _text_center', '{"type":"p","value":"Layanan 24 jam untuk menangani kendaraan dalam kondisi darurat selama 5 tahun oleh Astraworld"}', '', 3, 2, 11, NULL, NULL),
	(10, NULL, NULL, '{"src":"Logo Home Service.png","href":"google.com"}', NULL, 4, 1, 12, NULL, NULL),
	(11, NULL, '_sg_line_color_2 _text_center', '{"type":"p","value":"Layanan servis kendaraan di lokasi yang telah disepakati dengan customer"}', NULL, 3, 2, 12, NULL, NULL),
	(12, NULL, NULL, '{"src":"Logo PMP.png","href":"google.com"}', NULL, 4, 1, 13, NULL, NULL),
	(13, NULL, '_sg_line_color_2 _text_center', '{"type":"p","value":"Free Servis berkala selama 5 tahun atau 60.000 km (mana yang dicapai terlebih dahulu)"}', NULL, 3, 2, 13, NULL, NULL),
	(14, NULL, NULL, '{"src":"Logo Warranty.png","href":"google.com"}', NULL, 4, 1, 14, NULL, NULL),
	(15, NULL, '_sg_line_color_2 _text_center', '{"type":"p","value":"Jaminan garansi kendaraan selama 5 tahun atau 100.000 km (mana yang dicapai terlebih dahulu)"}', NULL, 3, 2, 14, NULL, NULL),
	(16, NULL, NULL, '{"type":"h3","value":"PEUGEOT BODY & PAINT <hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 18, NULL, NULL),
	(17, NULL, NULL, '{"type":"p","value":"Body & Paint Astra Peugeot merupakan satu-satunya bengkel authorized body repair yang khusus menangani perbaikan body dan pengecatan kendaraan bermotor yang dilengkapi peralatan sesuai standar fasilitas yang ditetapkan oleh Peugeot."}', NULL, 3, 1, 19, NULL, NULL),
	(18, NULL, NULL, '{"type":"p","value":"Bengkel Body & Paint Astra Peugeot juga melayani seluruh merk kendaraan selain merk Peugeot."}', NULL, 3, 1, 20, NULL, NULL),
	(19, NULL, NULL, '{"type":"p","value":"Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 21, NULL, NULL),
	(20, NULL, NULL, '{"type":"p","value":"See the location on maps <a class=\'_btn _button _primary _icon _bordered _sg_bg_color_2 _sg_line_color_1\'  href=\'\'><span class=\'fa fa-chevron-right\'></a>"}', NULL, 3, 2, 21, NULL, NULL),
	(21, NULL, NULL, '{"type":"h2","value":"PRODUCT"}', NULL, 2, 1, 15, NULL, NULL),
	(22, NULL, 'section_preview_nav active', '{"href":"javascript:void(0)","value":"3008 SUV"}', NULL, 7, 1, 16, NULL, NULL),
	(23, NULL, 'section_preview_nav', '{"href":"javascript:void(0)","value":"5008 SUV"}', NULL, 7, 2, 16, NULL, NULL),
	(24, NULL, 'section_preview_slider_tab active', '{"href":"javascript:void(0)","value":"Exterior"}', NULL, 7, 1, 17, NULL, NULL),
	(25, NULL, 'section_preview_slider_tab', '{"href":"javascript:void(0)","value":"Interior"}', NULL, 7, 2, 17, NULL, NULL),
	(26, 'sliderProductNavigate', 'sliderProductNavigate', '{"0":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUV"}]},"1":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUS"}]},"2":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUR"}]},"3":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"4":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"5":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]},"6":{"src":"slider-home-1-section1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h4","text":"PEUGEOT SUN"}]}}', 'slider-7', 6, 1, 23, NULL, NULL),
	(27, NULL, NULL, '{"type":"h2","value":"DEALER <hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 24, NULL, NULL),
	(28, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>ASTRA PEUGEOT SUNTER<br> JL YOS SUDARSO KAV 24 SUNTER II SUNTER 14330 NORTH JAKARTA<br> 0062216512325"}', NULL, 3, 1, 25, NULL, NULL),
	(29, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 26, NULL, NULL),
	(30, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 27, NULL, NULL),
	(31, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 28, NULL, NULL),
	(32, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>Astra Peugeot Body & Paint<br>JL. Pahlawan Seribu Perum BSD Blok 405 No 2-2A<br>Serpong - Tangerang Selatan<br>0215380011"}', NULL, 3, 1, 29, NULL, NULL),
	(33, NULL, NULL, '{"type":"h2","value":"EXIBITION <hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 30, NULL, NULL),
	(34, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-circle\'></span>ASTRA PEUGEOT SUNTER<br> JL YOS SUDARSO KAV 24 SUNTER II SUNTER 14330 NORTH JAKARTA<br> 0062216512325"}', NULL, 3, 2, 30, NULL, NULL),
	(35, NULL, NULL, '{"type":"p","value":"See the location on maps <a class=\'buttonLink-section-exibition\'  href=\'\'><span class=\'fa fa-chevron-right\'></a>"}', NULL, 3, 3, 30, NULL, NULL),
	(36, NULL, NULL, '{"src":"mapsLocation.png","href":"google.com"}', NULL, 4, 1, 31, NULL, NULL),
	(37, NULL, NULL, '{"type":"h2","value":"Event & Promotion"}', NULL, 2, 1, 32, NULL, NULL),
	(38, NULL, NULL, '{"type":"p","value":"Latest News"}', NULL, 3, 1, 33, NULL, NULL),
	(39, NULL, NULL, '{"type":"p","value":"Other News"}', NULL, 3, 1, 34, NULL, NULL),
	(40, NULL, NULL, '{"src":"news1.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<button>Read More</button>"]}', NULL, 12, 1, 35, NULL, NULL),
	(41, NULL, NULL, '{"src":"news2.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"NEW PEUGEOT 5008 PREMIUM SUV 7–SEATER RESMI MELUNCUR","additional":["Astra International Tbk – Peugeot Sales Operation (Astra Peugeot) selaku Agen Pemegang Merek dan Distributor Peugeot di Indonesia meluncurkan New Peugeot.","<button>Read More</button>"]}', NULL, 12, 2, 35, NULL, NULL),
	(42, NULL, NULL, '{"src":"news3.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 3, 35, NULL, NULL),
	(43, NULL, NULL, '{"src":"news4.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 4, 35, NULL, NULL),
	(44, NULL, NULL, '{"src":"news2.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<button>Read More</button>"]}', NULL, 12, 5, 35, NULL, NULL),
	(45, NULL, NULL, '{"src":"news2.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"NEW PEUGEOT 5008 PREMIUM SUV 7–SEATER RESMI MELUNCUR","additional":["Astra International Tbk – Peugeot Sales Operation (Astra Peugeot) selaku Agen Pemegang Merek dan Distributor Peugeot di Indonesia meluncurkan New Peugeot.","<button>Read More</button>"]}', NULL, 12, 6, 35, NULL, NULL),
	(46, NULL, NULL, '{"src":"news3.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 7, 35, NULL, NULL),
	(47, NULL, NULL, '{"src":"news4.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"LOREM IPSUM DOLOR SIT AMET","additional":["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,","<button>Read More</button>"]}', NULL, 12, 8, 35, NULL, NULL),
	(48, NULL, NULL, '{"type":"h1","value":"Style Guide"}', NULL, 2, 1, 36, NULL, NULL),
	(49, NULL, NULL, '{"type":"h2","value":"Typografi"}', NULL, 2, 1, 37, NULL, NULL),
	(50, NULL, NULL, '{"type":"h1","value":"H1 - This is Heading 55px"}', NULL, 2, 3, 37, NULL, NULL),
	(51, NULL, NULL, '{"type":"h2","value":"H2 - This is Heading 40px"}', NULL, 2, 4, 37, NULL, NULL),
	(52, NULL, NULL, '{"type":"h3","value":"H3 - This is Heading 31px"}', NULL, 2, 5, 37, NULL, NULL),
	(53, NULL, NULL, '{"type":"h4","value":"H4 - This is Heading 27px"}', NULL, 2, 6, 37, NULL, NULL),
	(54, NULL, NULL, '{"type":"h5","value":"H5 - This is Heading 22px"}', NULL, 2, 7, 37, NULL, NULL),
	(55, NULL, NULL, '{"type":"h6","value":"H6 - This is Heading 20px"}', NULL, 2, 8, 37, NULL, NULL),
	(56, NULL, NULL, '{"type":"p","value":"Heading - Helvetica Neue <hr>"}', NULL, 3, 2, 37, NULL, NULL),
	(57, NULL, NULL, '{"type":"p","value":"Paragraph - 16px <hr>"}', NULL, 3, 9, 37, NULL, NULL),
	(58, NULL, NULL, '{"type":"p","value":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sollicitudin nisi quis vehicula tempus. Aenean egestas ullamcorper est, eu dignissim quam sodales ut. Proin nec odio nunc. Nullam a magna vitae neque lobortis lacinia in eu dolor. In aliquam metus vel lacinia ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse congue nisl nec odio posuere egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut justo felis, congue dictum dignissim in, porta faucibus felis."}', NULL, 3, 10, 37, NULL, NULL),
	(59, NULL, NULL, '{"type":"h2","value":"Buttons"}', NULL, 2, 1, 38, NULL, NULL),
	(60, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-right\'></span>"}', NULL, 7, 3, 38, NULL, NULL),
	(61, NULL, '_button _primary _sm _bordered', '{"href":"","value":"BUTTON"}', NULL, 7, 4, 38, NULL, NULL),
	(62, NULL, '_button _primary _lg', '{"href":"","value":"BUTTON"}', NULL, 7, 5, 38, NULL, NULL),
	(63, NULL, '_button _secondary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-right\'></span>"}', NULL, 7, 8, 38, NULL, NULL),
	(64, NULL, '_button _secondary _sm _bordered', '{"href":"","value":"BUTTON"}', NULL, 7, 9, 38, NULL, NULL),
	(65, NULL, '_button _secondary _lg', '{"href":"","value":"BUTTON"}', NULL, 7, 10, 38, NULL, NULL),
	(66, NULL, NULL, '{"type":"h2","value":"Colors"}', NULL, 2, 1, 39, NULL, NULL),
	(67, NULL, '_sg_bg_color_1', '{"type":"p","value":"<span class=\'_sg_line_color_2\'>Aa</span> <span class=\'rgb\'>RGB : 19,39,76</span><span class=\'hex\'>Hex : #13274C</span>"}', NULL, 3, 2, 39, NULL, NULL),
	(68, NULL, '_sg_bg_color_2', '{"type":"p","value":"<span class=\'_sg_line_color_1\'>Aa</span> <span class=\'rgb\'>RGB : 255,255,255</span><span class=\'hex\'>Hex : #ffffff</span>"}', NULL, 3, 3, 39, NULL, NULL),
	(69, NULL, '_sg_bg_color_3', '{"type":"p","value":"<span class=\'_sg_line_color_2\'>Aa</span> <span class=\'rgb\'>RGB : 0,0,0</span><span class=\'hex\'>Hex : #000000</span>"}', NULL, 3, 4, 39, NULL, NULL),
	(72, NULL, NULL, '{"type":"p","value":"Primary <hr>"}', NULL, 3, 2, 38, NULL, NULL),
	(73, NULL, NULL, '{"type":"p","value":"Secondary <hr>"}', NULL, 3, 7, 38, NULL, NULL),
	(74, NULL, '_link _text-icon _primary', '{"href":"","value":"<span class=\'fa fa-plus\'></span>Links & Icon"}', NULL, 8, 6, 38, NULL, NULL),
	(75, NULL, '_link _text-icon _secondary', '{"href":"","value":"<span class=\'fa fa-plus\'></span>Links & Icon"}', NULL, 8, 11, 38, NULL, NULL),
	(76, NULL, NULL, '{"type":"h2","value":"Icons"}', NULL, 2, 1, 40, NULL, NULL),
	(77, NULL, NULL, '{"type":"h2","value":"Card"}', NULL, 2, 1, 41, NULL, NULL),
	(78, NULL, NULL, '{"type":"h2","value":"Images & Videos"}', '', 2, 1, 42, NULL, NULL),
	(79, '', NULL, '{"type":"h2","value":"Slider"}', NULL, 2, 1, 43, NULL, NULL),
	(80, NULL, NULL, '{"src":"news1.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<button class=\'_btn _primary _sm\'>Read More</button>"]}', 'card-1', 12, 2, 41, NULL, NULL),
	(81, NULL, '_img _block', '{"src":"mapsLocation.png","href":"google.com"}', NULL, 4, 2, 42, NULL, NULL),
	(82, NULL, '_videoPlayer', '{"src":"https://www.youtube.com/embed/WPQ0pE0PtSw?version=3&autoplay=1&controls=1&&showinfo=0&loop=1","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', 'video-1', 5, 3, 42, NULL, NULL),
	(83, '', '', '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 4, 42, NULL, NULL),
	(85, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-caret-right\'></span><span class=\'fa fa-search\'>search</span>"}', NULL, 3, 3, 40, NULL, NULL),
	(86, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-caret-right\'></span><span class=\'fa fa-chevron-right\'>chevron-right</span>"}', NULL, 3, 2, 40, NULL, NULL),
	(87, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-caret-right\'></span><span class=\'fa fa-plus\'>plus</span>"}', NULL, 3, 4, 40, NULL, NULL),
	(88, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-caret-right\'></span><span class=\'fa fa-registered\'>registered</span>"}', NULL, 3, 5, 40, NULL, NULL),
	(89, NULL, NULL, '{"type":"p","value":"<span class=\'fa fa-caret-right\'></span><span class=\'fa fa-times\'>times</span>"}', NULL, 3, 6, 40, NULL, NULL),
	(90, NULL, 'sliderHome', '{"0":{"src":"engine 1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"1":{"src":"news1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"2":{"src":"news2.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"3":{"src":"news3.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENHANCED HANDLING"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]}}', 'slider-9', 6, 3, 43, NULL, NULL),
	(91, NULL, 'sliderHome type-1', '{"0":{"src":"news1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"1":{"src":{"img":"coverVideo-cockpit.png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"2":{"src":"news2.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"3":{"src":"news2.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"4":{"src":{"img":"coverVideo-cockpit.png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"5":{"src":"news3.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"6":{"src":"news1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"7":{"src":{"img":"coverVideo-cockpit.png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"8":{"src":"news2.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]}}', 'slider-10', 6, 5, 43, NULL, NULL),
	(92, NULL, NULL, '{"type":"p","value":"Slider-9 <hr>"}', NULL, 3, 2, 43, NULL, NULL),
	(93, NULL, NULL, '{"type":"p","value":"Slider-10 <hr>"}', NULL, 3, 4, 43, NULL, NULL),
	(94, NULL, 'sliderHome', '{"0":{"src":"engine 1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"1":{"src":"news1.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"2":{"src":"news2.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"3":{"src":"news3.png","href":"https://gotrips.lk/site/images/uploads/img.jpg","heading":[{"tag":"h3","text":"ENHANCED HANDLING"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]}}', 'slider-11', 6, 7, 43, NULL, NULL),
	(95, NULL, NULL, '{"type":"p","value":"Slider-11 <hr>"}', NULL, 3, 6, 43, NULL, NULL),
	(96, NULL, NULL, '{"type":"p","value":"Paragraph - 11px <hr>"}', NULL, 3, 11, 37, NULL, NULL),
	(97, NULL, '_paragraph_small', '{"type":"p","value":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sollicitudin nisi quis vehicula tempus. Aenean egestas ullamcorper est, eu dignissim quam sodales ut. Proin nec odio nunc. Nullam a magna vitae neque lobortis lacinia in eu dolor. In aliquam metus vel lacinia ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse congue nisl nec odio posuere egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut justo felis, congue dictum dignissim in, porta faucibus felis."}', NULL, 3, 12, 37, NULL, NULL),
	(98, NULL, '_sg_bg_color_4', '{"type":"p","value":"<span class=\'_sg_line_color_1\'>Aa</span> <span class=\'rgb\'>RGB : 196,196,196</span><span class=\'hex\'>Hex : #C4C4C4</span>"}', NULL, 3, 5, 39, NULL, NULL),
	(99, NULL, NULL, '{"src":"news1.png","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<button class=\'_btn _primary _sm\'>Read More</button>"]}', 'card-2', 12, 3, 41, NULL, NULL),
	(100, NULL, 'sliderProduct', '{"0":{"src":"PEUGEOT_5008_2019_010_UK 1.png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUV"}],"description":[{"tag":"H2","text":"5008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"820.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"1":{"src":"PEUGEOT_5008_2019_010_UK 1.png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUS"}],"description":[{"tag":"H2","text":"5008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"820.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"2":{"src":"PEUGEOT_5008_2019_010_UK 1.png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUR"}],"description":[{"tag":"H2","text":"5008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"820.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"3":{"src":"PEUGEOT_5008_2019_010_UK 1.png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUN"}],"description":[{"tag":"H2","text":"5008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"820.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}}}', 'slider-5', 6, 1, 44, NULL, NULL),
	(101, NULL, '_sg_line_color_1', '{"type":"h4","value":"A NEW WAY OF THINKING"}', NULL, 2, 1, 45, NULL, NULL),
	(102, NULL, '_sg_line_color_1', '{"type":"p","value":"Bearing the same name and outstanding 7-seat practicality of its predecessor, we are proud to introduce the Peugeot 5008 SUV. Re-designed from the ground up, Peugeot 5008 SUV makes an instant impact with a sharp exterior design. Details including LED daytime running lights, chrome accents and stylish alloy wheels feature at all levels on this stylish SUV."}', NULL, 3, 2, 45, NULL, NULL),
	(103, NULL, '_sg_line_color_1', '{"type":"h4","value":"FREEDOM REINVENTED"}', NULL, 2, 3, 47, NULL, NULL),
	(104, NULL, '_sg_line_color_1', '{"type":"p","value":"Whether alone or with passengers, your experience on board the 7-seater Peugeot 5008 SUV is taken to a whole new level. Enjoy beautiful, natural light from the opening panoramic glass roof which floods the luxurious interior cabin. Relax with the multi-point massage seats, customise your interior ambience with PEUGEOT i-Cockpit® Amplify and enjoy the premium Hi-Fi system by Focal®."}', NULL, 3, 4, 47, NULL, NULL),
	(105, NULL, '_sg_line_color_1', '{"type":"h4","value":"AN ENHANCED DRIVING EXPERIENCE"}', NULL, 2, 5, 49, NULL, NULL),
	(106, NULL, '_sg_line_color_1', '{"type":"p","value":"Awaken your senses on board the Peugeot 5008 SUV with the new PEUGEOT i-Cockpit® and its high-tech environment. The 12.3” head-up digital instrument panel, 8.0\' capacitive touchscreen, compact multifunction steering wheel and electric impulse automatic gearbox control* offer a unique and immersive driving experience. The new PEUGEOT i-Cockpit® centres around a driving experience that puts you in control. The 12.3” head-up digital instrument panel is fully customisable, gone are the days of being constrained to conventional analogue dials. The display is also positioned above the steering wheel, closer to the road, increasing safety. The compact multifunction steering wheel has been designed to increase driving pleasure and agility, its compact dimensions make light work of manoeuvres. Finally the 8” capacitive touchscreen with multi-touch technology is angled towards the driver for increased comfort."}', NULL, 3, 6, 49, NULL, NULL),
	(107, NULL, '_sg_line_color_4', '{"type":"h1","value":"NEW <br/>5008 SUV"}', NULL, 2, 1, 46, NULL, NULL),
	(108, NULL, '', '{"src":"product-new-5008-suv-advantage.png"}', NULL, 4, 1, 48, NULL, NULL),
	(109, NULL, '_sg_line_color_2 _text_center', '{"type":"h2","value":"EXTERIOR DESIGN"}', NULL, 2, 1, 50, NULL, NULL),
	(110, NULL, '_sg_line_color_2 _text_center', '{"type":"h2","value":"EXTERIOR DESIGN"}', NULL, 2, 1, 51, NULL, NULL),
	(111, NULL, '_sg_line_color_2', '{"type":"h4","value":"PEUGEOT i-COCKPIT®"}', NULL, 2, 1, 52, NULL, NULL),
	(112, NULL, '_sg_line_color_2', '{"type":"p","value":"AN ENHANCED DRIVING EXPERIENCE <br/><br/>With its spectacular new PEUGEOT i-Cockpit, the Peugeot 5008 SUV invites you to explore a 100% digital world. The configurable 8\' capacitive touchscreen with smart phone levels of responsiveness, configurable 12.3\' head-up digital instrument panel which can be personalised to suit the drivers needs and the compact sports steering wheel with integrated controls are guaranteed to enhance your driving experience."}', NULL, 3, 2, 52, NULL, NULL),
	(113, NULL, NULL, '{"src":"PEUGEOT_5008_2016_198_FR 1.png"}', NULL, 4, 3, 52, NULL, NULL),
	(114, NULL, NULL, '{"src":"1PP7CMPCRKB0IYB0_0MM00NDZ_0P610RFX_010 1.png"}', NULL, 4, 1, 53, NULL, NULL),
	(115, NULL, '_sg_line_color_2', '{"type":"p","value":"INTUITION AT YOUR FINGERTIPS<br/><br/> To accompany the new PEUGEOT i-Cockpit®, versions equipped with an Efficient Automatic Transmission (EAT6) are operated by the stylish and ergonomically designed Electric Impulse Control. Beautiful in its design, carefully selected materials including satin chrome, full grain leather and high gloss piano black trim combine to give the control an appearance most fitting of the new PEUGEOT i-Cockpit®. Offering as much substance as style, the Electric Impulse Control can automatically engage ‘Park’ when the ignition is switched off, giving the driver one less thing to think about."}', NULL, 3, 2, 53, NULL, NULL),
	(116, NULL, '_sg_line_color_2', '{"type":"h2","value":"CONNECTIVITY"}', NULL, 2, 1, 130, NULL, NULL),
	(117, NULL, '_sg_line_color_2', '{"type":"p","value":"Mirror Screen®<br/><br/> Functionality To ensure seamless compatibility between your smartphone and Peugeot 5008 SUV every model benefits from Mirror Screen® functionality*. Mirror Screen® encompasses Apple CarPlay™, MirrorLink® and Android Auto technologies. With a compatible device you will be able to enjoy key features from your smartphone mirrored onto the 8.0” capacitive touchscreen. In addition to charging via USB, a smartphone charging plate** can also be specified to allow wireless inductive charging on the move.<br/><br/> <span class=\'_paragraph_small\'>*The Mirror Screen® function operates, as appropriate, via MirrorLink® technology (for MirrorLink®-compatible Android, Blackberry and Windows phones), via Apple CarPlay™ (for iOS phones) or via Android Auto (for Android phones) subject to holding a telephone subscription including Internet access with the user\'s operator. For more information, visit peugeot.co.uk for more details <br/><br/>** Optional on Active and Allure versions - Induction charging for devices compatible with the Qi standard</span>"}', NULL, 3, 2, 130, NULL, NULL),
	(118, NULL, NULL, '{"src":"PEUGEOT_3008_2016_284_UK 1.png"}', NULL, 4, 1, 131, NULL, NULL),
	(119, NULL, NULL, '{"src":"PEUGEOT_5008_2016_195_FR 1.png"}', NULL, 4, 1, 134, NULL, NULL),
	(120, NULL, '_sg_line_color_2', '{"type":"h2","value":"INTERIOR COMFORT"}', NULL, 2, 1, 135, NULL, NULL),
	(121, NULL, '_sg_line_color_2', '{"type":"p","value":"ENJOY THE WORLD OUTSIDE<br/><br/>Bathe the cabin in natural light with the opening panoramic glass roof*. With tilt and slide functions, this large glass roof can open up to sixteen inches. Complete with mood lighting that illuminates the full length of the glass panel at night and an electric sunblind for when the UK sun proves to be a bit too much.<br/><br/> <span class=\'_paragraph_small\'>* Optional on Active, Allure & GT Line versions</span>"}', NULL, 3, 2, 135, NULL, NULL),
	(122, NULL, '_sg_line_color_2', '{"type":"p","value":"AWAKEN YOUR SENSES <br/><br/>Enhance your experience on board Peugeot 5008 SUV in a number of ways…. Enjoy a relaxing driver seat multipoint massage* with five massage types conducted through eight-pocket pneumatic technology. To enhance the interior further, why not increase the intensity of the mood lighting**. With lighting in the door pockets, centre console, roof*** and in a cascading form down the inside of the front door panels, this results in a real night time spectacle. <br/><br/><span class=\'_paragraph_small\'>* As an option on Allure versions ** Standard on Allure versions *** As an option on Active and Allure versions</span> <br/><br/>HI-FI SOUND SYSTEM <br/><br/>Check out the amazing acoustics inside the Peugeot 5008 SUV with the premium Hi-Fi system from Focal®* with 10 built-in speakers for pure and sharp acoustics. <br/><br/><span class=\'_paragraph_small\'>*Optional from Allure onwards</span>"}', NULL, 3, 1, 137, NULL, NULL),
	(123, NULL, NULL, '{"src":"PEUGEOT_5008_2016_418_FR 1.png"}', NULL, 4, 1, 138, NULL, NULL),
	(124, NULL, '_sg_line_color_2 _text_center', '{"type":"h3","value":"ROAD PERFORMANCE"}', NULL, 2, 1, 65, NULL, NULL),
	(125, NULL, NULL, '{"src":"1PP7CMPCRKB0IYB0_0MM60NN9_0P610RFX_002 1.png"}', NULL, 4, 2, 65, NULL, NULL),
	(126, NULL, '_sg_line_color_1 _text_center', '{"type":"p","value":"A 5-STAR EURO NCAP RATING<br/><br/> Peugeot 5008 SUV has achieved the highest possible rating of 5 stars in the <br/>2017 Euro NCAP testing."}', NULL, 3, 3, 65, NULL, NULL),
	(127, NULL, '_sg_line_color_2', '{"type":"h2","value":"EXTERIOR DESIGN <hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 58, NULL, NULL),
	(128, NULL, '_sg_line_color_2', '{"type":"h3","value":"BOLD LINES"}', NULL, 2, 2, 58, NULL, NULL),
	(129, NULL, '_sg_line_color_2', '{"type":"p","value":"With a purposeful stance and raised ride height Peugeot 5008 SUV makes its intentions clear. Impactful design touches are everywhere to be seen, from the striking front end design to the black diamond roof*. In profile, the contrasting wheel arch trim, stainless steel roof arch, aluminum roof bars and large alloy wheels, ranging from 17” to 19”** enhance the look even further. <br/><br/><span class=\'_paragraph_small\'>* Optional on Active and Allure versions **19”‘Washington’ alloy wheel optional on Allure versions</span>"}', NULL, 3, 3, 58, NULL, NULL),
	(130, NULL, NULL, '{"src":"Mask Group.png"}', NULL, 4, 4, 58, NULL, NULL),
	(131, NULL, NULL, '{"src":"Mask Group (1).png"}', NULL, 4, 1, 59, NULL, NULL),
	(132, NULL, '_sg_line_color_2', '{"type":"h3","value":"A STRIKING FRONT"}', NULL, 2, 2, 59, NULL, NULL),
	(133, NULL, '_sg_line_color_2', '{"type":"p","value":"Discover the striking front end featuring a prominent chrome grille with decorative chrome inserts. Signature headlamps with LED daytime running light technology, feline in their appearance, complement the design beautifully. A raised horizontal bonnet and vertical front face add to the car’s presence. Go a step further with Allure versions and enjoy a front bumper adorned with a scuff plate finished in distinctive ‘Lion Grey’."}', NULL, 3, 3, 59, NULL, NULL),
	(134, NULL, NULL, '{"src":"Mask Group (2).png"}', NULL, 4, 4, 59, NULL, NULL),
	(135, NULL, '_link _text-icon _primary', '{"href":"","value":"<span class=\'fa fa-plus\'></span><br/>LEARN MORE ABOUT MODULAR LAYOUT"}', NULL, 8, 1, 54, NULL, NULL),
	(136, NULL, '_sg_line_color_1', '{"src":"PEUGEOT_5008_2016_180_FR 1.png","href":"","title":"OUTSTANDING MODULARITY","additional":["The all-new Peugeot 5008 SUV is ready for whatever you can throw at it. With seven individual seats on board, everyone can get comfortable. Second row occupants can enjoy longitudinal adjustment to maximise legroom and even recline the seat back rest angle for optimum comfort on each of the three independent seats. Third row occupants will also enjoy generous levels of leg and headroom. If not required the third row seats can be removed and stored separately"]}', 'card-3', 12, 1, 55, NULL, NULL),
	(137, NULL, '_sg_line_color_1', '{"src":"Mask Group (3).png","href":"","title":"A SPACIOUS BOOT","additional":["Hands full? Watch the ingenious Smart Electric Tailgate* open and close, making it easy to load all your objects. All it takes is a swift foot motion under the bumper centre to activate it. With a large boot aperture and low load sill, loading the Peugeot 5008 SUV couldn’t be easier too. Whether it be a couple of surf boards after a trip to the beach or the family pet for a trip to the park, this large SUV will take it in its stride. <br/><br/><span class=\'_paragraph_small\'>* Linked option on Allure versions</span>"]}', 'card-3', 12, 1, 56, NULL, NULL),
	(138, NULL, '_sg_line_color_1', '{"src":"Mask Group (4).png","href":"","title":"OUTSTANDING MODULARITY","additional":["With seven independent seats, Peugeot 5008 SUV offers outstanding flexibility, with row three lowering so you can enjoy an impressive load space of 952 litres. The third row seats weigh just 11kg each and can be removed from the vehicle and stored separately, increasing the load space to 1060 litres. Fold the second row flat and this increases the load space to a truly cavernous 2,150 litres. For extra long loads, simply fold the front passenger seat* to increase the load length to 3.2 metres. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure versions upwards</span>"]}', 'card-3', 12, 1, 57, NULL, NULL),
	(139, NULL, '_sg_line_color_2', '{"type":"h3","value":"THE PERFECT FINISH"}', NULL, 2, 5, 58, NULL, NULL),
	(140, NULL, '_sg_line_color_2', '{"type":"p","value":"The aerodynamic contours of the slanted rear window beautifully highlight the sporty style. Visible by day and at night, the three-claw LED lights are integrated into the bodywork’s rear black panel to create the unique signature of the Peugeot 5008 SUV. For a sporting touch a rear spoiler enhances the silhouette further whilst Allure models also enjoy dark tinted rear windows for an increased feeling of exclusivity."}', NULL, 3, 6, 58, NULL, NULL),
	(141, NULL, '_link _text-icon _sg_line_color_2', '{"href":"","value":"<span class=\'fa fa-times\'></span> CLOSE"}', NULL, 8, 1, 60, NULL, NULL),
	(142, NULL, 'sliderHome', '{"0":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"1":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"2":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"3":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"ENHANCED HANDLING"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]}}', 'slider-9', 6, 1, 140, NULL, NULL),
	(143, NULL, 'sliderProduct', '{"0":{"src":"Mask Group (15).png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUV"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}},"1":{"src":"Mask Group (15).png","href":"","heading":[{"tag":"h4","text":"PEUGEOT SUV"}],"description":[{"tag":"H2","text":"3008 GT LINE"},{"tag":"h4","text":"OTR DKI JAKARTA"},{"tag":"h3","text":"730.000.000 IDR"}],"additional":{"button":[{"href":"","id":"","class":"","value":"CONTACT US"}],"paragraph":[{"type":"p","value":"<span>5 years warranty</span> or 100.000 km"},{"type":"p","value":"<span>5 years free periodical maintenance (parts included)</span> or 60.000 km"}]}}}', 'slider-5', 6, 1, 67, NULL, NULL),
	(144, NULL, '_sg_line_color_1', '{"type":"h4","value":"INSPIRED STYLING"}', NULL, 2, 1, 68, NULL, NULL),
	(145, NULL, '_sg_line_color_1', '{"type":"p","value":"Fall in love with the powerful front of the PEUGEOT 3008 SUV, with its stylish chrome detail and full LED headlights* adding to the sleek, feline look. Discover the car\'s fluid and dynamic lines, further emphasised by a strong shoulder line running the length of this striking SUV. The sharp design continues at the rear, thanks to its Black Diamond roof* and gloss black rear panel incorporating the PEUGEOT signature claw effect LED lighting. <br/><br/><span class=\'_paragraph_small _sg_line_color_4\'>*Standard on GT Line and GT</span>"}', NULL, 3, 2, 68, NULL, NULL),
	(146, NULL, '_sg_line_color_4', '{"type":"h1","value":"NEW <br/>3008 SUV"}', NULL, 2, 1, 69, NULL, NULL),
	(147, NULL, '_sg_line_color_1', '{"type":"h4","value":"AN ENHANCED EXPERIENCE"}', NULL, 2, 1, 70, NULL, NULL),
	(148, NULL, '_sg_line_color_1', '{"type":"p","value":"Drivers have never before had such a beautiful space for all their adventures. With its spectacular new PEUGEOT i-Cockpit®, the PEUGEOT 3008 SUV invites you to discover a 100% digital universe. The 8.0\'\' capacitive touchscreen, configurable 12.3\'\' head-up digital display instrument panel, compact steering wheel featuring integrated controls and electronic automatic transmission control system are the perfect combination to enhance your driving experience."}', NULL, 3, 2, 70, NULL, NULL),
	(149, NULL, '_sg_line_color_1', '{"type":"h4","value":"INNOVATIVE TECHNOLOGY"}', NULL, 2, 1, 72, NULL, NULL),
	(150, NULL, '_sg_line_color_1', '{"type":"p","value":"We should explore the world but also respect it using environmentally-efficient technologies. The lighter chassis, BlueHDi and turbo petrol engines and EAT6 automatic gearbox mean the PEUGEOT 3008 SUV is among the most efficient SUVs in its class. Like a sixth sense when on the road, the PEUGEOT 3008 SUV’s arsenal of driving aids gives a remarkable feeling of safety, including Active Safety Brake with pedestrian detection, Active Lane Departure Warning System*, Active Blind Spot Monitoring System* and Adaptive cruise control with stop feature*. Combine this with 360° Birdview camera**, Park Assist** and High Beam Assist* and you have complete peace of mind in the 3008 SUV. <br/><br/><span class=\'_paragraph_small _sg_line_color_4\'>*Standard on Allure, GT Line and GT<br/>**Standard on Allure, GT Line and GT. Active gets 180° reversing camera</span>"}', NULL, 3, 2, 72, NULL, NULL),
	(151, NULL, '_sg_line_color_1', '{"type":"h4","value":"THE PEUGEOT 3008 SUV: MORE THAN 30 AWARDS WON"}', NULL, 2, 3, 72, NULL, NULL),
	(152, NULL, '_sg_line_color_1', '{"type":"p","value":"Since its launch, the PEUGEOT 3008 SUV has won more than 30 international awards, including the prestigious 2017 European Car of the Year Award. The PEUGEOT 3008 SUV has seduced many juries around the world thanks to its dynamic qualities, stylish exterior, equipment, interior design and of course its innovative PEUGEOT i-Cockpit®."}', NULL, 3, 4, 72, NULL, NULL),
	(153, NULL, NULL, '{"src":"Mask Group (5).png","href":"","title":"FROM DUST TO ROADS","additional":["We keep going back to the Dakar Rally every year for the countless number of lessons we learn from the toughest rally raid in the world. And all of these lessons and innovations have been incorporated into our PEUGEOT SUV range.",""]}', 'card-2', 12, 1, 74, NULL, NULL),
	(154, NULL, '_link _text-icon _primary', '{"href":"","value":"<span class=\'fa fa-plus\'></span><br/>LEARN MORE ABOUT MODULAR LAYOUT"}', NULL, 8, 1, 73, NULL, NULL),
	(155, NULL, 'sliderHome type-1', '{"0":{"src":"Mask Group (6).png","href":"","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]},"1":{"src":{"img":"Mask Group (7).png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":""},{"tag":"P","text":""}]},"2":{"src":"Mask Group (8).png","href":"","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]},"3":{"src":"Mask Group (6).png","href":"","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]},"4":{"src":{"img":"Mask Group (7).png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":""},{"tag":"P","text":""}]},"5":{"src":"Mask Group (8).png","href":"","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]},"6":{"src":"Mask Group (6).png","href":"","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]},"7":{"src":{"img":"Mask Group (7).png","embed":"https://www.youtube.com/embed/WPQ0pE0PtSw"},"href":"","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":""},{"tag":"P","text":""}]},"8":{"src":"Mask Group (8).png","href":"","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":""},{"tag":"P","text":""},{"tag":"P","text":""}]}}', 'slider-10', 6, 1, 75, NULL, NULL),
	(156, NULL, '_button _primary _sm _sg_bg_color_3', '{"href":"#","value":"LENGTH"}', NULL, 7, 1, 78, NULL, NULL),
	(157, NULL, '_button _primary _sm _sg_bg_color_3', '{"href":"#","value":"INTERIOR"}', NULL, 7, 2, 78, NULL, NULL),
	(158, NULL, '_button _primary _sm _sg_bg_color_3', '{"href":"#","value":"BOOT"}', NULL, 7, 3, 78, NULL, NULL),
	(159, NULL, '_sg_line_color_2 _text_center', '{"type":"h2","value":"DIMENSIONS"}', NULL, 2, 1, 77, NULL, NULL),
	(160, NULL, NULL, '{"src":"Group 86.png"}', NULL, 4, 1, 79, NULL, NULL),
	(161, NULL, 'sliderHome', '{"0":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"ENGINE"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"1":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"GEAR BOX"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"2":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"SAFETY"}],"description":[{"tag":"h4","text":"PURETECH PETROL ENGINES"},{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]},"3":{"src":"engine 1.png","href":"","heading":[{"tag":"h3","text":"ENHANCED HANDLING"}],"description":[{"tag":"P","text":"Offered with the EAT6 6-speed automatic gearbox, the 121kW THP turbo petrol engine* boasts a high level of torque (240 Nm @ 1400 rpm) including in lower gears. It combines dynamism and flexibility. Its 121kW of power offers impressive acceleration. This Euro 6-compliant engine excels in terms of performance and driving pleasure, as well as lowering fuel consumption and CO2 emissions."},{"tag":"P","text":"*Available on Active, Allure and GT Line"}]}}', 'slider-9', 6, 1, 82, NULL, NULL),
	(162, NULL, NULL, '{"src":"Mask Group (9).png"}', NULL, 4, 1, 83, NULL, NULL),
	(163, NULL, '_sg_line_color_2', '{"type":"h4","value":"POWER AND ELEGANCE"}', NULL, 2, 1, 84, NULL, NULL),
	(164, NULL, '_sg_line_color_2', '{"type":"p","value":"The PEUGEOT 3008 SUV instantly provides an air of presence with its finely balanced proportions. To allow you to express yourself, choose from one of the following two distinctive front end designs: <br/><br/>- A radiator grille with chrome facets, surrounded by sharp and bright halogen headlights – on Active and Allure levels <br/><br/>- A chiselled chequered radiator grille combined with full LED headlights for a sleek, feline look – on GT Line and GT levels <br/><br/>The PEUGEOT 3008\'s SUV robust look continues with its raised ride height strong shoulder line and horizontal bonnet, while the stylish chrome wing inserts and stainless steel roof arch trim emphasise its fluid and dynamic lines."}', NULL, 3, 2, 84, NULL, NULL),
	(165, NULL, '_sg_line_color_2', '{"type":"h4","value":"A DISTINCT DESIGN"}', NULL, 2, 1, 85, NULL, NULL),
	(166, NULL, '_sg_line_color_2', '{"type":"p","value":"The PEUGEOT 3008 SUV asserts a unique and sharp design. <br/><br/>Its streamlined shape is enhanced by the gloss Black Diamond roof*. The rear of the car continues this iconic and innovative style, with a gloss black rear panel incorporating PEUGEOT \'s signature claw effect LED lighting. <br/><br/><span class=\'_paragraph_small\'>*Standard on GT Line and GT</span>"}', NULL, 3, 2, 85, NULL, NULL),
	(167, NULL, NULL, '{"src":"Mask Group (10).png"}', NULL, 4, 3, 85, NULL, NULL),
	(168, NULL, NULL, '{"src":"Mask Group (11).png"}', NULL, 4, 1, 86, NULL, NULL),
	(169, NULL, '_sg_line_color_2', '{"type":"h4","value":"BREATHTAKINGLY BEAUTIFUL INTERIOR STYLE"}', NULL, 2, 2, 86, NULL, NULL),
	(170, NULL, '_sg_line_color_2', '{"type":"p","value":"Taking your seat behind the wheel of PEUGEOT 3008 SUV will allow you to discover our most advanced version of the new PEUGEOT i-Cockpit® along with its high-tech innovations. <br/><br/>The steering wheel is more compact and the large 8.0\'\' capacitive touch screen resembles a tablet installed in the centre of the dashboard. The 12.3\'\' head-up digital display can be completely configured and personalised, and brings together all that the driver could want within their field of vision. <br/><br/>To complement the 8.0\'\' touch screen satin chrome toggle switches inspired by the world of aviation provide a unique touch to the striking cockpit design, with perfect weighting and damping, each switch is a work of art. Each switch gives direct access to the main control functions: radio, climate control, 3D Navigation, vehicle parameters, telephone and mobile applications."}', NULL, 3, 3, 86, NULL, NULL),
	(171, NULL, NULL, '{"src":"Mask Group (12).png"}', NULL, 4, 1, 87, NULL, NULL),
	(172, NULL, '_sg_line_color_2', '{"type":"h4","value":"ATTENTION TO DETAIL"}', NULL, 2, 1, 88, NULL, NULL),
	(173, NULL, '_sg_line_color_2', '{"type":"p","value":"Every detail of the new PEUGEOT I-Cockpit® has been designed following a meticulous approach, from the full-grain leather trimmed steering wheel to the satin chrome detail contrasting with the high gloss black trim and the soft touch materials that adorn the door cards and dashboard facia, no detail has been overlooked."}', NULL, 3, 2, 88, NULL, NULL),
	(174, NULL, '_sg_line_color_2', '{"type":"h4","value":"MODULAR LAYOUT"}', NULL, 2, 1, 89, NULL, NULL),
	(175, NULL, '_sg_line_color_2', '{"type":"p","value":"The folding bench seat has a 2/3 - 1/3 \'Magic Flat\' capability to optimise loading with a flat floor so you can create an interior space to suit you. To access the \'Magic Flat\' function, a simple pull of the release switches in the boot is all that is required, to extend the already impressive bootspace from 591 litres to the full 1670 litres of capacity available (including spare wheel well). The excellent levels of modularity continue further for the loading of lengthy items. At the pull of a lever the front passenger seat folds flat*, allowing the loading of longer items to be easier than ever. <br/><br/><span class=\'_paragraph_small\'>*Not available on Active</span>"}', NULL, 3, 2, 89, NULL, NULL),
	(176, NULL, NULL, '{"src":"Mask Group (13).png"}', NULL, 4, 1, 90, NULL, NULL),
	(177, NULL, NULL, '{"src":"Mask Group (14).png"}', NULL, 4, 1, 91, NULL, NULL),
	(178, NULL, '_sg_line_color_2', '{"type":"h4","value":"WHEELS"}', NULL, 2, 1, 92, NULL, NULL),
	(179, NULL, '_sg_line_color_2', '{"type":"p","value":"To perfectly complement the striking SUV design, PEUGEOT 3008 SUV is available with a variety of stylish alloy wheels design, ranging from 17\'\' \'Chicago\' alloy wheels on Active models to 18\'\' \'Los Angeles\' alloy wheels on Allure and GT Line (with Mud & Snow tyres in conjunction with Grip Control) to 19\'\' \'Boston\' alloy wheels on GT. The Advanced Grip Control option* comes with the 18\'\' \'Los Angeles\' alloy wheel. *Optional on GT"}', NULL, 3, 2, 92, NULL, NULL),
	(180, NULL, '_sg_line_color_2', '{"type":"h4","value":"SEAT TRIMS"}', NULL, 2, 1, 93, NULL, NULL),
	(181, NULL, '_sg_line_color_2', '{"type":"p","value":"Put together a warm and elegant ambience which suits you with our selection of interior fabric or leather seat trims."}', NULL, 3, 2, 93, NULL, NULL),
	(182, NULL, NULL, '{"src":"1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_011 1.png"}', NULL, 4, 1, 94, NULL, NULL),
	(183, NULL, '_sg_line_color_2 _text_center', '{"type":"h3","value":"ROAD PERFORMANCE"}', NULL, 2, 1, 95, NULL, NULL),
	(184, NULL, NULL, '{"src":"1PP8SYPCRKB0IYB1_0MM00NF4_0P610RFX_004 1.png"}', NULL, 4, 2, 95, NULL, NULL),
	(185, NULL, '_sg_line_color_1 _text_center', '{"type":"p","value":"A 5-STAR EURO NCAP RATING<br/><br/> Peugeot 5008 SUV has achieved the highest possible rating of 5 stars in the <br/>2017 Euro NCAP testing."}', NULL, 3, 3, 95, NULL, NULL),
	(186, NULL, NULL, '{"src":"1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_001 1.png"}', NULL, 4, 1, 71, NULL, NULL),
	(187, NULL, NULL, '{"src":"PEUGEOT_3008_2016_193_FR 1.png","href":"","title":"EXEMPLARY HANDLING","additional":["With a raised driving position and increased ground clearance, the PEUGEOT 3008 SUV gives you a commanding view of the road ahead.",""]}', 'card-2', 12, 1, 96, NULL, NULL),
	(188, NULL, NULL, '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 2, 97, NULL, NULL),
	(189, NULL, '', '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 1, 97, NULL, NULL),
	(190, NULL, NULL, '{"src":"PEUGEOT_3008_2016_703_FR 1.png"}', NULL, 4, 1, 99, NULL, NULL),
	(191, NULL, '_sg_line_color_1', '{"type":"h4","value":"ELECTRIC IMPULSE AUTOMATIC GEARBOX CONTROL"}', NULL, 2, 1, 100, NULL, NULL),
	(192, NULL, '_sg_line_color_1', '{"type":"p","value":"Linked to the Efficient Automatic Transmission (EAT6) with Quickshift Technology, this stylish gearbox control with its ergonomic design complements the PEUGEOT i-Cockpit®. Trimmed with full-grain leather, satin chrome and piano black materials, this compact gearbox control even helps to free up space within the cabin. Operation is intuitive and smooth, and for the more exuberant driver a manual driving mode is also available. A simple press of the \'M\' button located on the control gives full access to the steering column mounted paddles."}', NULL, 3, 2, 100, NULL, NULL),
	(193, NULL, NULL, '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 1, 101, NULL, NULL),
	(194, NULL, '_sg_line_color_2', '{"type":"h4","value":"3D NAVIGATION"}', NULL, 2, 1, 102, NULL, NULL),
	(195, NULL, '_sg_line_color_2', '{"type":"p","value":"Satellite Navigation: Linked to both the 8.0\'\' capacitive colour touchscreen and the 12.3\'\' head-up digital instrument panel, the system includes Australian mapping, 3D city mapping (where available) and voice recognition (for radio and telephone functions)."}', NULL, 3, 2, 102, NULL, NULL),
	(196, NULL, NULL, '{"src":"Mask Group (17).png"}', NULL, 4, 1, 103, NULL, NULL),
	(197, NULL, NULL, '{"src":"Mask Group (18).png"}', NULL, 4, 1, 104, NULL, NULL),
	(198, NULL, NULL, '{"src":"Mask Group (19).png"}', NULL, 4, 2, 104, NULL, NULL),
	(199, NULL, '_sg_line_color_2', '{"type":"h4","value":"MIRROR SCREEN® AND SMARTPHONE CHARGING PLATE"}', NULL, 2, 1, 105, NULL, NULL),
	(200, NULL, '_sg_line_color_2', '{"type":"p","value":"Allows the mirroring of a compatible smartphone screen to access the key functionality on the handset. Once paired functions are displayed on the 8.0\'\' capacitive colour touchscreen. PEUGEOT Mirror Screen® uses the following systems: <br/><br/>Apple CarPlay® : Just plug in your iPhone to make calls, send and recieve messages, listen to music and get directions. Functions can either be controlled via the touchscreen or Siri voice control. <br/><br/>Mirror Link®: Offering similar functionality to CarPlay®, Mirror Link® allows the seamless pairing of a compatible Android phone to access to navigation, music and phone apps. <br/><br/>Smartphone Charging Plate*: <br/><br/>Through the use of inductive charging technology a compatible device can be charged wirelessly whilst on the move. Operation is simple: just place your compatible smartphone on the charging area and once detected, battery charging begins. <br/><br/>*The device must be Qi-compatible, the very latest smartphones from Blackberry, Google, HTC, LG, Microsoft, Motorola, Samsung and Sony feature this technology built in with no additional add-on required. Other devices including iPhone 5/5c/5s, iPhone SE and IPhone 6/6 Plus/6s/6s Plus will require an additional QInside Qi2001 accessory to access the wireless charging functionality. For more information on this growing technology please visit the Qi website."}', NULL, 3, 2, 105, NULL, NULL),
	(201, NULL, NULL, '{"src":"https://www.youtube.com/embed/WPQ0pE0PtSw?version=3&autoplay=1&controls=1&&showinfo=0&loop=1","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', 'video-1', 5, 1, 106, NULL, NULL),
	(202, NULL, NULL, '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 2, 106, NULL, NULL),
	(203, NULL, NULL, '{"src":"https://www.youtube.com/embed/WPQ0pE0PtSw?version=3&autoplay=1&controls=1&&showinfo=0&loop=1","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', 'video-1', 5, 1, 107, NULL, NULL),
	(204, NULL, NULL, '{"0":{"heading":{"type":"h2","value":"<span class=\'fa fa-plus\'></span><br>LEARN MORE ABOUT<br> NEXT GENERATION PEUGEOT i-COCKPIT<span class=\'fa fa-registered\'></span>"},"body":{"value":"<b>Configurable 12.3 head-up digital instrument panel</b><br> Available as standard across the PEUGEOT 3008 SUV range, this 12.3 100% digital touchscreen can be customised to suit the driver\'s needs. As the panel is positioned higher, it is closer to the road and is more within the driver\'s field of vision. This can increase safety when compared to a conventional set-up where the dials sit within the wheel, further from the road."}}}', 'accordion-1', 10, 2, 107, NULL, NULL),
	(205, NULL, '_sg_line_color_2', '{"type":"h4","value":"HANDS-FREE ELECTRIC TAILGATE"}', NULL, 2, 1, 108, NULL, NULL),
	(206, NULL, '_sg_line_color_2', '{"type":"p","value":"To make loading and unloading that little bit easier, the PEUGEOT 3008 SUV with hands-free electric tailgate* technology can help take a load off your mind. Featuring a sensor located under the rear of the vehicle, a simple foot movement underneath the bumper is all that is required (vehicle key must be within range) to open and close the tailgate. <br/><br/>In addition, the tailgate can be electrically operated using the rear handle, tailgate interior facia panel, dashboard and keyless entry enabled key fob. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure, GT Line and GT</span>"}', NULL, 3, 2, 108, NULL, NULL),
	(207, NULL, NULL, '{"src":"https://www.youtube.com/embed/WPQ0pE0PtSw?version=3&autoplay=1&controls=1&&showinfo=0&loop=1","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', 'video-1', 5, 1, 109, NULL, NULL),
	(208, NULL, NULL, '{"src":"PEUGEOT_3008_2016_085_FR 1.png"}', NULL, 4, 1, 110, NULL, NULL),
	(209, NULL, '_sg_line_color_2', '{"type":"h4","value":"LED TECHNOLOGY"}', NULL, 2, 1, 111, NULL, NULL),
	(210, NULL, '_sg_line_color_2', '{"type":"p","value":"The PEUGEOT 3008 SUV has elegant headlights with black and chrome trim, which further emphasise the new SUV\'s modern, feline look. These headlights incorporate LED daytime running lights, and full LED headlights*. <br/><br/>The 3D-effect LED claw lights at the rear of the vehicle make an instant impact. These lights are visible by day and at night and give the PEUGEOT 3008 SUV its powerful identity and impressive presence on the road. <br/><br/>The benefits of LED technology: <br/><br/>- Consumes considerably less energy to produce light, 50% less than halogen and 35% less than xenon technologies. <br/><br/>- Offers a whiter light for improved visibility at night, the light is also more similar to daylight, leading to reduced driver eye fatigue, improving safety. <br/><br/>- Requires no warm up time with instantaneous lighting response when compared to conventional halogen technologies. <br/><br/>- LED technology will last for the life of the car, compared to halogen technology with much shorter life expectancy. <br/><br/><span class=\'_paragraph_small\'>*Standard on GT Line and GT</span>"}', NULL, 3, 2, 111, NULL, NULL),
	(211, NULL, '_sg_line_color_2', '{"type":"h4","value":"PANORAMIC OPENING GLASS ROOF"}', NULL, 2, 1, 112, NULL, NULL),
	(212, NULL, '_sg_line_color_2', '{"type":"p","value":"Enjoy the world outside with the PEUGEOT 3008 SUV. Divided into two glass panels that bathe the cabin in natural light, the panoramic opening glass roof* enhances the appearance of this stylish SUV. <br/><br/>For an added breath of fresh air the glass panel includes a tilt function and can be opened up to 16 inches. An electric roller blind is also included, in either Salin light grey or Mistral black, dependent on level. <br/><br/>To enhance the style even further, blue ambient lighting flanks the glass panel. <br/><br/><span class=\'_paragraph_small\'>*Optional on GT Line and GT as part of Premium Pack</span>"}', NULL, 3, 2, 112, NULL, NULL),
	(213, NULL, NULL, '{"src":"https://www.youtube.com/embed/WPQ0pE0PtSw?version=3&autoplay=1&controls=1&&showinfo=0&loop=1","href":"https:\\/\\/wallpapermemory.com\\/uploads\\/200\\/mount-bromo-wallpaper-hd-1920x1200-320096.jpg"}', 'video-1', 5, 1, 113, NULL, NULL),
	(214, NULL, '_sg_line_color_1', '{"type":"h4","value":"PANORAMIC OPENING GLASS ROOF"}', NULL, 2, 1, 114, NULL, NULL),
	(215, NULL, '_sg_line_color_1', '{"type":"p","value":"<b>Active Safety Brake</b> <br/><br/> An intelligent autonomous braking system designed to avoid a collision or limit its severity by reducing the vehicle\'s speed. The system is triggered if the driver does not react quickly enough, by activating the vehicle\'s brakes. <br/><br/>A multifunction camera located at the top of the windscreen coupled with a front bumper mounted radar are able to detect obstructions on the road. The system is able to detect vehicles that are moving in the same direction, or are at a stop (up to 80km/h), together with pedestrians in the road (up to 60km/h). <br/><br/><b>Distance Alert System</b><br/><br/> Working in conjunction with the active safety brake, the distance alert function warns the driver if the vehicle risks a collision with the vehicle in front or a pedestrian in the road. Two levels of warning are available, the first \'Amber\' warning provides a visual alert to the potential risk. In a more a severe instance a \'Red\' visual and audible alert indicate a collision is imminent and driver action is required. Where fitted, failure to act triggers the active safety brake for added peace of mind."}', NULL, 3, 2, 114, NULL, NULL),
	(216, NULL, NULL, '{"src":"Mask Group (20).png"}', NULL, 4, 1, 115, NULL, NULL),
	(217, NULL, NULL, '{"src":"Mask Group (21).png"}', NULL, 4, 1, 116, NULL, NULL),
	(218, NULL, '_sg_line_color_1', '{"type":"h4","value":"PANORAMIC OPENING GLASS ROOF"}', NULL, 2, 1, 117, NULL, NULL),
	(219, NULL, '_sg_line_color_1', '{"type":"p","value":"Cruise control as you know it, will never be the same again with PEUGEOT 3008 SUV and EAT6 automatic gearbox technology. Controlled via a radar mounted at the front of the vehicle with a range of up to 150 metres, the Active Cruise Control Stop system* provides two key functions: <br/><br/>Automatic maintenance of the vehicle\'s speed, at the value set by the driver. <br/><br/>Automatic maintenance of a set distance between the PEUGEOT 3008 SUV and the vehicle in front, slowing the vehicle to a stop if required. <br/><br/>The system detects a vehicle driving in front in the same direction and can automatically adapt the vehicle\'s speed to that of the vehicle in front. This occurs using engine braking and the braking system (in which case the brake lights are lit) to maintain a constant distance. <br/><br/>If the vehicle in front has slowed to a stop, PEUGEOT 3008 SUV will also be brought to a stop by the ACC Stop system, if after this no response is received from the driver, the electric parking brake is automatically applied. This intelligent function is available at speeds of 30km/h upwards. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure, GT Line and GT</span>"}', NULL, 3, 2, 117, NULL, NULL),
	(220, NULL, '_sg_line_color_1', '{"type":"h4","value":"LANE KEEPING TECHNOLOGY"}', NULL, 2, 1, 118, NULL, NULL),
	(221, NULL, '_sg_line_color_1', '{"type":"p","value":"The PEUGEOT 3008 SUV is always one step ahead. Featuring two lane keeping technologies, the vehicle is able to identify lines on the road via a windscreen mounted camera. The technologies are as follows: <br/><br/>- Lane Departure Warning: A system that monitors continuous and discontinuous lines and warns the driver if a longitudinal lane marking on the ground is accidentally crossed. If steering deviation is detected, you are alerted by a flashing indicator on the digital instrument panel and an audible signal in the cabin. <br/><br/>- Active Lane Keeping Assistance*: As soon as the system identifies a risk of accidentally crossing one of the marking lines detected on the ground, the steering is gradually corrected to keep the vehicle in lane, at speeds of over 64km/h. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure, GT Line and GT</span>"}', NULL, 3, 2, 118, NULL, NULL),
	(222, NULL, NULL, '{"src":"Screen Shot 2019-11-23 at 23.13 1.png"}', NULL, 4, 1, 119, NULL, NULL),
	(223, NULL, NULL, '{"src":"PEUGEOT_3008_2018_021_UK 1.png"}', NULL, 4, 1, 120, NULL, NULL),
	(224, NULL, '_sg_line_color_1', '{"type":"h4","value":"DRIVER ATTENTION ALERT"}', NULL, 2, 1, 121, NULL, NULL),
	(225, NULL, '_sg_line_color_1', '{"type":"p","value":"To help you stay sharp and alert behind the wheel, the PEUGEOT 3008 SUV features a two stage driver alert system for added safety on the road, comprising: <br/><br/><b>Driver Attention Alert: </b>It is important to take frequent breaks when travelling over long distances. A timed alert system operates at speeds in excess of 64km/h, after two hours of driving a message is displayed recommending that you take a break. <br/><br/><b>Advanced Driver Attention Alert*: </b>Controlled by a camera at the top of the windscreen designed to analyse the vehicle\'s trajectory, this active system evaluates the vigilance of the driver at speeds of over 64km/h. By identifying deviations relative to the road markings the PEUGEOT 3008 SUV is able to determine whether the vehicle\'s behaviour reflects a certain degree of driver drowsiness or inattention. If it is felt that the driver needs to take better care on the road a visual and audible alert will occur. If more than three instances occur, the driver will be advised to take a break. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure, GT Line and GT</span>"}', NULL, 3, 2, 121, NULL, NULL),
	(226, NULL, NULL, '{"src":"Mask Group (22).png"}', NULL, 4, 1, 122, NULL, NULL),
	(227, NULL, '_sg_line_color_1', '{"type":"h4","value":"SPEED LIMIT RECOGNITION"}', NULL, 2, 1, 123, NULL, NULL),
	(228, NULL, '_sg_line_color_1', '{"type":"p","value":"Using the windscreen mounted camera, the PEUGEOT 3008 SUV features speed limit sign recognition, leaving you in no doubt over the speed limit. Upon detecting a speed limit sign this information is read by the camera and displayed in colour on the 12.3\'\' head-up digital instrument panel. <br/><br/>In addition to being made aware of the speed the driver also has the option to adapt their speed to the speed limit on the particular road via the vehicle\'s cruise control stalk. The vehicle will then adjust its speed accordingly."}', NULL, 3, 2, 123, NULL, NULL),
	(229, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT SMARTBEAM ASSISTANCE"}', NULL, 2, 1, 124, NULL, NULL),
	(230, NULL, '_sg_line_color_1', '{"type":"p","value":"PEUGEOT Smartbeam Assistance gives you one less thing to think about when driving at night. Controlled via a windscreen mounted camera, the system continuously analyses driving conditions, for instance the proximity of other vehicles. <br/><br/>PEUGEOT Smartbeam Assistance automatically switches headlamps to full beam when conditions allow, allowing you to tap into the full potential of the full LED headlamps** that offer uniform lighting to reduce eye fatigue, increasing driver comfort. <br/><br/>*Standard on Allure, GT Line and GT <br/><br/><span class=\'_paragraph_small\'>**Standard on GT Line and GT models</span>"}', NULL, 3, 2, 124, NULL, NULL),
	(231, NULL, NULL, '{"src":"Mask Group (23).png"}', NULL, 4, 1, 125, NULL, NULL),
	(232, NULL, NULL, '{"src":"Screen Shot 2019-11-23 at 23.53 1.png"}', NULL, 4, 1, 126, NULL, NULL),
	(233, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT SMARTBEAM ASSISTANCE"}', NULL, 2, 1, 127, NULL, NULL),
	(234, NULL, '_sg_line_color_1', '{"type":"p","value":"With the Active Blind Spot Monitoring system* now you can enjoy the feeling of having eyes in the back of your head. <br/><br/>Using lateral ultrasound sensors to cover areas out of your field of vision along with the windscreen mounted camera, an LED illuminates in either side door mirror to alert to the presence of a car, lorry, motorbike or bicycle in your blind spot. <br/><br/>The system works when overtaking a slower vehicle or when overtaken by a faster vehicle. This intelligent feature operates from speeds as low as 13km/h. <br/><br/><span class=\'_paragraph_small\'>*Standard on Allure, GT Line and GT</span>"}', NULL, 3, 2, 127, NULL, NULL),
	(235, NULL, NULL, '{"src":"1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_005 1.png"}', NULL, 4, 1, 128, NULL, NULL),
	(236, NULL, '_sg_line_color_1', '{"type":"h1","value":"NEW 3008 SUV"}', NULL, 2, 1, 129, NULL, NULL),
	(237, NULL, '_sg_line_color_1', '{"type":"h4","value":"Lorem ipsum dolor sit amet, consectetur adipiscing elit."}', NULL, 2, 2, 129, NULL, NULL),
	(238, NULL, '_site-logo', '{"src":"Logo.png"}', NULL, 4, 1, 143, NULL, NULL),
	(239, NULL, '_site-logo', '{"src":"Logo-typograf.png"}', NULL, 4, 2, 143, NULL, NULL),
	(240, NULL, '_site-icon-list _sg_line_color_2', '{"type":"p","value":"<span class=\'fa fa-bars\'></span>"}', '', 3, 3, 143, NULL, NULL),
	(241, NULL, '_nav-list _item-1', '{"type":"p","value":"product"}', '', 3, 1, 144, NULL, NULL),
	(242, NULL, '_nav-list _item-2', '{"type":"p","value":"after sales"}', NULL, 3, 2, 144, NULL, NULL),
	(243, NULL, '_sub-nav-', '', NULL, 0, 0, 0, NULL, NULL),
	(244, NULL, '_nav-list _item-3', '{"type":"p","value":"network"}', NULL, 3, 3, 144, NULL, NULL),
	(245, NULL, '_nav-list _item-4', '{"type":"p","value":"about us"}', NULL, 3, 4, 144, NULL, NULL),
	(246, NULL, '_sub-list _item-1 _sub-1', '{"href":"product/new-3008","value":"new 3008"}', NULL, 8, 1, 145, NULL, NULL),
	(247, NULL, '_sub-list _item-1 _sub-2', '{"href":"product/new-5008","value":"new 5008"}', NULL, 8, 2, 145, NULL, NULL),
	(248, NULL, '_preview-list-1 _item-1 _sub-1', '{"src":"/product/3008/1PP8SYPCRKB0IYB1_0MM60NN9_0P610RFX_004.png"}', NULL, 4, 1, 146, NULL, NULL),
	(249, NULL, '_preview-list-1 _item-1 _sub-1', '{"type":"p","value":"Named 2017 European Car of the Year, the PEUGEOT 3008 SUV unveils its strength and character. Featuring a sleek design, this distinct SUV combines robustness with elegance. Explore a unique sensory world on board the PEUGEOT 3008 SUV"}', NULL, 3, 2, 146, NULL, NULL),
	(250, NULL, '_preview-list-2 _item-1 _sub-2', '{"src":"/product/3008/1PP8SYPCRKB0IYB1_0MM00NLG_0P610RFX_004.png"}', NULL, 4, 1, 146, NULL, NULL),
	(251, NULL, '_preview-list-2 _item-1 _sub-2', '{"type":"p","value":"Named 2017 European Car of the Year, the PEUGEOT 3008 SUV unveils its strength and character. Featuring a sleek design, this distinct SUV combines robustness with elegance. Explore a unique sensory world on board the PEUGEOT 5008 SUV"}', NULL, 3, 2, 146, NULL, NULL),
	(252, NULL, NULL, '{"src":"shutterstock_1410848027 1.png"}', NULL, 4, 1, 147, NULL, NULL),
	(253, NULL, '_sg_line_color_2', '{"type":"p","value":"Peugeot staff are always there to listen to your needs. You can contact us directly by email, telephone or post. To be sure to receive all our latest news, follow us on the social media."}', NULL, 3, 2, 148, NULL, NULL),
	(254, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 3, 148, NULL, NULL),
	(255, NULL, '_sg_line_color_1', '{"type":"h2","value":"PEUGEOT INDONESIA <br/><hr/>"}', NULL, 2, 1, 149, NULL, NULL),
	(256, NULL, NULL, '{"src":"Group 88.png"}', NULL, 4, 1, 150, NULL, NULL),
	(257, NULL, NULL, '{"type":"p","value":"Call us Peugeot Hotline Service at 1-500 898 <br/>Telephone +62 21 650 5757 <br/> Facsimile +62 21 658 36824"}', NULL, 3, 1, 151, NULL, NULL),
	(258, NULL, NULL, '{"src":"Frame.png"}', NULL, 4, 1, 152, NULL, NULL),
	(259, NULL, NULL, '{"type":"p","value":"We are available to reply to all your questions. Email us now! <br/><br/><b>e-mail: info@peugeot.astra.co.id</b>"}', NULL, 3, 1, 153, NULL, NULL),
	(260, NULL, NULL, '{"src":"Frame (1).png"}', NULL, 4, 1, 154, NULL, NULL),
	(261, NULL, NULL, '{"type":"p","value":"PT. ASTRA INTERNATIONAL-PEUGEOT<br/> Jl. Yos Sudarso Kav. 24 Sunter II<br/> Jakarta Utara 14330 - Indonesia"}', NULL, 3, 1, 155, NULL, NULL),
	(262, NULL, NULL, '{"src":"peugeot-prestations-apres-vente-2015-01.9179 2.png"}', NULL, 4, 1, 156, NULL, NULL),
	(263, NULL, '_sg_line_color_2', '{"type":"p","value":"A PEUGEOT expert checks and repairs your vehicle. You get spare parts designed specifically for your PEUGEOT, competitively priced plans and the PEUGEOT network\'s attention to detail."}', NULL, 3, 1, 157, NULL, NULL),
	(264, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 2, 157, NULL, NULL),
	(265, NULL, '_sg_line_color_1', '{"type":"h2","value":"AFTER SALES SERVICES <br/><hr/>"}', NULL, 2, 1, 158, NULL, NULL),
	(266, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT HOTLINE"}', NULL, 2, 1, 159, NULL, NULL),
	(267, NULL, '_sg_line_color_1', '{"type":"p","value":"Standby 24 <br/><br/>At three in the morning, you glanced at his watch. The road is so quiet. Cars can not be the starter. No need to panic! Lock the doors and the phone immediately 1-500898 <br/><br/>We even have to think further. Senior mechanical PEUGEOT ALERT 24 experienced an emergency, come complete with workshop equipment and spare parts are adequate. So, if you are affected three in the morning, Be Calm ! <br/><br/>Wherever you are, PEUGEOT ALERT 24 PEUGEOT owners ready to serve in all regions of Java Island. <br/><br/>Record the pulse-free telephone number."}', NULL, 3, 2, 159, NULL, NULL),
	(268, NULL, NULL, '{"src":"Mask Group (24).png"}', NULL, 4, 1, 160, NULL, NULL),
	(269, NULL, NULL, '{"src":"Mask Group (25).png"}', NULL, 4, 1, 162, NULL, NULL),
	(270, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT WARRANTY"}', NULL, 2, 1, 163, NULL, NULL),
	(271, NULL, '_sg_line_color_1', '{"type":"p","value":"Peugeot assure you from begin.. <br/><br/>When you buy a Peugeot, you will get a comprehensive warranties on vehicles up to 5 years or 100,000 km whichever is reached first apply from 1 August 2019 for all types of vehicles. The warranty period is effective from vehicles handed over to customers. <br/><br/>Free of mind ... <br/><br/>With the Peugeot you can enjoy the convenience and worry-free driving."}', NULL, 3, 2, 163, NULL, NULL),
	(272, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT HOME SERVICE"}', NULL, 2, 1, 165, NULL, NULL),
	(273, NULL, '_sg_line_color_1', '{"type":"p","value":"Routine care for PEUGEOT. It certainly you know. But, if you are abusy and no longer time to your PEUGEOT? Do not to worries. No need to sacrifice precious time. Just use the exclusive service of this one. <br/><br/>PEUGEOT VISIT SERVICING . So easy! <br/><br/>Simply call and make an appointment. Senior mechanical PEUGEOT SERVICING go ready to go to handle your PEUGEOT as carefully. This is an opportunity for routine maintenance while you are in the middle of a business meeting. <br/><br/>Entrusting to care your PEUGEOT with PEUGEOT HOME SERVICE. <br/><br/>One more exclusive services that prove our commitment to you and your own PEUGEOT.<br/><br/>JABOTABEK <br/><br/>Sunter (021) 651-2325<br/><br/>Cilandak(021) 750-0334"}', NULL, 3, 2, 165, NULL, NULL),
	(274, NULL, NULL, '{"src":"Mask Group (26).png"}', NULL, 4, 1, 166, NULL, NULL),
	(275, NULL, NULL, '{"src":"Mask Group (27).png"}', NULL, 4, 1, 168, NULL, NULL),
	(276, NULL, '_sg_line_color_1', '{"type":"h4","value":"PEUGEOT MAINTENANCE PACKAGE"}', NULL, 2, 1, 169, NULL, NULL),
	(277, NULL, '_sg_line_color_1', '{"type":"p","value":"As the owner of Peugeot, you have the freedom of driving without any fear of the cost of regular maintenance and replacement of components. You will immediately benefit program Peugeot Maintenance Package at no extra cost. <br/><br/>Peugeot Maintenance Package Program will give customers the freedom to drive without fear of regular maintenance and replacement of components that can wear out due to use within a period of 5 years or up to 60,000 km, whichever comes first. <br/><br/>Peugeot Maintenance Package program will remain in effect if your vehicle has been sold or change hands until the deadline or kilometers predetermined. <br/><br/>Peugeot Maintenance Package to make sure your vehicle is maintained in accordance with the recommendation Peugeot consisting of regular maintenance and replacement of parts that could wear out due to usage using original spare parts, among others include: <br/><br/>Engine oil, including the oil filter. <br/>Maintenance or replacement pollen filter, air filter, fuel filter, spark plugs, brake pads, brake discs, and a drive belt. <br/>Fluid include brake fluid, power steering oil, and radiator coolant. <br/>Diagnosis ECU. <br/>The items below are not included in the program Peugeot Maintenance Package, including:<br/><br/> Top-up or add engine oil. <br/>repair / maintenance between two periodic maintenance specified in the book Peugeot Maintenance Package. <br/>Damage due to fuel contamination. <br/>Replacement tires and battery. <br/>Balancing the wheel. <br/>Installation or repair accessories. <br/>Damage due to accidents, fires, natural disasters (including damage due to ingress of water into the combustion chamber or water hammer), modification of vehicles, noise is not normal on the vehicle. <br/>Damage due to the user\'s responsibility, such as: participation in the race, engine damage due to lack of engine oil, etc. You can perform regular maintenance on the entire network of authorized workshops Peugeot and enjoy this program by showing the book Peugeot Maintenance Package."}', NULL, 3, 2, 169, NULL, NULL),
	(278, NULL, NULL, '{"src":"banner-news-peugeot-alt-3 1.png"}', NULL, 4, 1, 170, NULL, NULL),
	(279, NULL, '_sg_line_color_2', '{"type":"h2","value":"NEWS <br/><hr class=\'_sg_line_color_2\'/>"}', NULL, 2, 1, 171, NULL, NULL),
	(280, NULL, '_sg_line_color_2', '{"src":"news1.png","href":"news/detail","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<a href=\'news/detail\' class=\'_btn _primary _sm\'>Read More</a>"]}', 'card-1', 12, 1, 173, NULL, NULL),
	(281, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 1, 182, NULL, NULL),
	(282, NULL, NULL, '{"src":"banner.png"}', NULL, 4, 1, 183, NULL, NULL),
	(283, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 1, 184, NULL, NULL),
	(284, NULL, '_sg_line_color_1 ', '{"type":"h2","value":"Stars of The Hamburg Tennis Tournament <br/><hr class=\'_sg_line_color_1\'/>"}', NULL, 2, 1, 185, NULL, NULL),
	(285, NULL, '_sg_line_color_1 ', '{"type":"p","value":"As the ATP Tour’s Platinum Partner since 2016, PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28. Four PEUGEOT ambassadors will be wearing the brand\'s colours during the tournament: Alexander Zverev, Pablo Carreño Busta, Leonardo Mayer and Robin Haase. Two of the brand’s models will be on display: the new PEUGEOT 208 and the new PEUGEOT 508 SW HYBRID. This historic German tournament will be held from the 22nd to 28th July. Formerly called the German Open, it has been going since 1892, and this year will see Alexander Zverev play for the 5th time. <br/><br/>The number 5 in the world is tackling the challenge on the courts of his hometown. Alongside him, Pablo Carreño Busta (No. 50), Leonardo Mayer (No. 59) and Robin Haase (No. 76) will face the world\'s other top players on the clay courts at the historic Rothenbaum Stadium in Hamburg, Germany. These four prestigious players, of German, Spanish, Argentinean or Dutch nationality, are part of the ambassador team that the PEUGEOT brand formed in 2017 to extend the international deployment of its sponsorship strategy in the tennis world. As the Partner and Official Car of the tournament, PEUGEOT will be providing players and VIPs in Hamburg with a fleet of 12 vehicles including the new PEUGEOT 508 sedan and SW, SUV PEUGEOT 3008 and 5008, and the PEUGEOT Traveller. <br/><br/>The brand is also displaying the new PEUGEOT 208 in Hamburg, in its combustion version and in its launch colour Faro Yellow. PEUGEOT has been enduring the transformations of the automotive world since 1810. Because the energy transition is a real opportunity, the new PEUGEOT 208 will be available as soon as it is launched in electric, petrol or diesel versions. Perfectly in line with the PEUGEOT brand\'s move upmarket, this new generation of PEUGEOT 208 stands out thanks to its sharp design, injecting both youth and energy. Innovative through its distinctive style, the new PEUGEOT 208 is also innovating with a new generation of the PEUGEOT i-Cockpit© with 3D head-up combination and a range of driving aids from the upper segments. <br/><br/>The new PEUGEOT 508 SW HYBRID will also be featured at the German tournament. The new PEUGEOT 508 SW challenges the codes of the disputed D-segment station wagon market and draws its inspiration from the world of shooting brakes. With its low and dynamic silhouette, the new PEUGEOT 508 SW displays a striking design: it combines athletic elegance and practicality in terms of spaciousness, volume and boot accessibility.<br/><br/> The new PEUGEOT 508 SW HYBRID combines the 180hp/132kW PureTech engine with a 110hp/80kW electric motor for a maximum combined power of 225hp/165kW, in traction. It is equipped with a sophisticated engine to offer ever more sensations. It has optimised its operating costs thanks to the highest level of efficiency with CO2 emissions standing at less than 49g of CO2 per WLTP(1) kilometre. The traction chain was installed without compromise. The spaciousness of the PEUGEOT 508 SW is maintained at the same level and the boot volume is identical to the combustion version, i.e. 530L. This new engine, which will be launched in early 2020, is concrete proof of PEUGEOT\'s vision of unconstrained, exciting and ever more efficient mobility.<br/><br/><b>#UnboringTheFuture.</b><br/> (1) The values given are in accordance with the WLTP procedure. As from 1 September 2018, new vehicles shall be approved on the basis of the Worldwide Harmonised Light Vehicle Test Procedure (WLTP), which is a new and more realistic test procedure to measure the fuel consumption, CO2 emissions and range of plug-in hybrid vehicles. This WLTP procedure, performed under more realistic test conditions, completely replaces the New European Driving Cycle (NEDC), which was the test procedure used previously. <br/><br/>The range measured for plug-in hybrid vehicles under the WLTP procedure is, very often, lower than that measured under the NEDC procedure. The autonomy values indicated correspond to the average values of the vehicles of the range. All values may vary depending on specific equipment, options and tyre types."}', NULL, 3, 2, 185, NULL, NULL),
	(286, NULL, '_sg_line_color_1 ', '{"type":"h4","value":"OTHER NEWS<br/><hr class=\'_sg_line_color_1\'/>"}', NULL, 2, 1, 189, NULL, NULL),
	(287, NULL, NULL, '{"src":"news1.png","href":"news/detail","title":"Stars of The Hamburg Tennis Tournament","additional":["PEUGEOT will be present at the Master 500 in Hamburg as a Partner and Official Car of the tournament, to be held from July 22 to 28","<a href=\'news/detail\' class=\'_btn _primary _sm\'>Read More</a>"]}', 'card-1', 12, 1, 186, NULL, NULL),
	(288, NULL, NULL, '{"src":"peugeot-quick-service.179030 1.png"}', NULL, 4, 1, 190, NULL, NULL),
	(289, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 1, 191, NULL, NULL),
	(290, NULL, '_sg_line_color_1 ', '{"type":"h2","value":"SERVICE<br/><hr class=\'_sg_line_color_1\'/>"}', NULL, 2, 1, 192, NULL, NULL),
	(291, NULL, '_sg_line_color_1 ', '{"type":"h5","value":"Every 20,000 km → Fuel System Cleaning - Gasoline"}', NULL, 2, 1, 193, NULL, NULL),
	(292, NULL, '_sg_line_color_1 ', '{"type":"p","value":"<ul><li>Increase the acceleration and engine power </li><li>Remove dirt on the injectors, valve and air ducts </li><li>Improve fuel economy</li></ul><br/><b>For All Types → Rp 400.000, -</b>"}', NULL, 3, 2, 193, NULL, NULL),
	(293, NULL, NULL, '{"src":"shutterstock_1542990557 1.png"}', NULL, 4, 1, 199, NULL, NULL),
	(294, NULL, '_button _primary _icon', '{"href":"","value":"<span class=\'fa fa-chevron-down\'></span>"}', NULL, 7, 1, 200, NULL, NULL),
	(295, NULL, '_sg_line_color_1 ', '{"type":"h2","value":"DEALER<br/><hr class=\'_sg_line_color_1\'/>"}', NULL, 2, 1, 201, NULL, NULL),
	(296, NULL, NULL, '{"src":"Rectangle 112.png","href":"news/detail","title":"ASTRA PEUGEOT SUNTER","additional":["JL YOS SUDARSO KAV 24 SUNTER II SUNTER 14330 NORTH JAKARTA <br/><br/>+6221 6512 325","<hr class=\'_sg_line_color_2 \'/>","<a href=\'news/detail\'>See the location on maps</a>"]}', 'card-1', 12, 1, 202, NULL, NULL);
/*!40000 ALTER TABLE `components` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css
CREATE TABLE IF NOT EXISTS `css` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projects` int(11) NOT NULL DEFAULT '0',
  `code` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css: ~1 rows (approximately)
DELETE FROM `css`;
/*!40000 ALTER TABLE `css` DISABLE KEYS */;
INSERT INTO `css` (`id`, `projects`, `code`, `created_at`, `updated_at`) VALUES
	(1, 1, 'hr{\r\n    border: .5px solid #13274C;\r\n}\r\nh1,h2,h3,h4,h5,h6,p {\r\n    margin-top: 10px;\r\n    margin-bottom: 10px;\r\n}\r\nh1{\r\n    font-size: 55px;\r\n}\r\nh2{\r\n    font-size: 40px;\r\n}\r\nh3{\r\n    font-size: 31px;\r\n}\r\nh4{\r\n    font-size: 27px;\r\n}\r\nh5{\r\n    font-size: 22px;\r\n}\r\nh6{\r\n    font-size: 20px;\r\n}\r\np{\r\n    font-size: 16px;\r\n    line-height:24px;\r\n}\r\n._paragraph_small{\r\nfont-size: 12px;\r\nline-height: 15px;\r\n}\r\n\r\n._text_left {\r\ntext-align: left;\r\n}\r\n\r\n._text_center {\r\ntext-align: center;\r\n}\r\n\r\n._text_right {\r\ntext-align: right;\r\n}\r\n\r\n._text_justify {\r\ntext-align: justify;\r\n}\r\n\r\n._sg_line_color_1{\r\n   color:#13274C !important;\r\n   border-color: #13274C !important;\r\n}\r\n._sg_line_color_2{\r\n   color:#ffffff !important;\r\n   border-color: #ffffff !important;\r\n}\r\n._sg_line_color_3{\r\n   color:#000000 !important;\r\n   border-color: #000000 !important;\r\n}\r\n._sg_line_color_4{\r\n   color:#C4C4C4 !important;\r\n   border-color: #C4C4C4 !important;\r\n}\r\n._sg_bg_color_1{\r\n   background-color:#13274C !important;\r\n}\r\n._sg_bg_color_2{\r\n      background-color:#ffffff !important;\r\n}\r\n._sg_bg_color_3{\r\n      background-color:#000000 !important;\r\n}\r\n._sg_bg_color_4{\r\n   background-color:#C4C4C4 !important;\r\n}\r\n\r\n._section-style-guide ._container ._grid:nth-child(2){\r\n    width: 50%;\r\n    margin-right: 15px;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(3){\r\n    width: 25%;\r\n    margin-right: 15px;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(4){\r\n    width: 20%;\r\n    margin-right: 15px;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(5){\r\n    width: 21%;\r\n    margin-right: 15px;\r\n    float: left;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(6){\r\n    width: 48%;\r\n    margin-right: 15px;\r\n    float: left;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(7){\r\n    width: 60%;\r\n    margin-right: 15px;\r\n    float: left;\r\n    margin-left: -140px;\r\n}\r\n._section-style-guide ._container ._grid:nth-child(8){\r\n    width: 100%;\r\n    float: left;\r\n    margin-right: 15px;\r\n}\r\n._section-style-guide ._container ._typografi{\r\n    margin:0;\r\n}\r\n._section-style-guide ._container {\r\n    padding: 70px 50px;\r\n    font-family: Helvetica Neue, Arial, sans-serif;\r\n}\r\n._section-style-guide ._container ._buttons ._btn{\r\n    margin: 10px 10px;\r\n}\r\n._bordered{\r\n   border-radius : 6px;\r\n}\r\n._btn{\r\n    display:block;\r\n   font-size : 14px;\r\n    padding: 10px 0px;\r\n    text-align : center;\r\n    text-decoration: none;\r\n    border: unset;\r\n}\r\n._btn._primary{\r\n    background : #13274C;\r\n    color : #ffffff;\r\n    transition: color,background .2s ease-out;\r\n}\r\n._btn._primary:hover{\r\n    background : #051738;\r\n    transition: color,background .2s ease-out;\r\n}\r\n._btn._secondary{\r\n    background : #ffffff;\r\n    color : #13274C;\r\n    transition: color,background .2s ease-out;\r\n}\r\n._btn._secondary:hover{\r\n    background : #13274C;\r\n    color : #ffffff;\r\n    transition: color,background .2s ease-out;\r\n}\r\n._btn._sm{\r\n    width: 168px;\r\n}\r\n._btn._lg{\r\n    width: 258px;\r\n}\r\n._btn._icon{\r\n    width: fit-content;\r\n   padding: 9.5px 13.5px;\r\n}\r\n._container ._buttons ._link{\r\n    text-decoration:none;   \r\n}\r\n._container ._buttons ._text-icon .fa{\r\n    margin-right: 10px;\r\n    border: 1px solid;\r\n    padding: 3px 5px;\r\n    border-radius: 15pc;\r\n}\r\n._buttons ._link._primary{\r\n    color: #13274C;\r\n}\r\n._buttons ._link._secondary{\r\n    color: #e8e8e8;\r\n}\r\n._colors p{\r\n    width: 30px;\r\n    text-align: center;\r\n    height: 30px;\r\n    padding: 18px 11px 4px;\r\n    box-shadow: 5px 5px 10px lightgrey;\r\n    border-radius: 15pc;\r\n}\r\n._section-style-guide ._container ._colors p:nth-of-type(1){\r\n    background-color: #13274C;\r\n    color: white;\r\n}\r\n._section-style-guide ._container ._colors p:nth-of-type(2){\r\n    background-color: #ffffff;\r\n}\r\n._section-style-guide ._container ._colors p:nth-of-type(3){\r\n    background-color: #000000;\r\n    color: white;\r\n}\r\n._section-style-guide ._container ._colors p span {\r\n    position: absolute;\r\n    margin-left: 30px;\r\n    margin-top: -10px;\r\n    color: black;\r\n}\r\n._section-style-guide ._container ._colors p span:nth-child(1) {\r\n    margin-left: -10px;\r\n    margin-top: -5px;\r\n}\r\n._section-style-guide ._container ._colors p .rgb {\r\n    padding-top: 17px;\r\n}\r\n\r\n._section-style-guide ._container ._image-videos ._img{\r\n    height: 255px;\r\n}\r\n._img{\r\n    margin: 0;\r\n    padding: 0;\r\n    object-fit: cover;\r\n}\r\n._img._block{\r\n    width: 100%;\r\n}\r\n._section-style-guide ._container ._image-videos embed{\r\n    height:300px;\r\n}\r\n._section-style-guide ._container ._image-videos .accordion-1{\r\n    margin-top: -4px;\r\n}\r\n\r\n@media (max-width: 1200px) and (min-width: 993px) {\r\n}\r\n\r\n@media (max-width: 992px) and (min-width: 769px) {\r\n}\r\n\r\n@media (max-width: 768px) and (min-width: 426px) {\r\n}\r\n\r\n@media (max-width: 425px) and (min-width: 321px){\r\n}\r\n\r\n@media (max-width: 320px) and (min-width: 0px) {\r\n}', '2019-10-22 11:58:36', '2019-10-22 11:58:36');
/*!40000 ALTER TABLE `css` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css_responsive
CREATE TABLE IF NOT EXISTS `css_responsive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `css` int(11) NOT NULL DEFAULT '0',
  `code` text NOT NULL,
  `max-width` int(11) NOT NULL DEFAULT '0',
  `min-width` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css_responsive: ~0 rows (approximately)
DELETE FROM `css_responsive`;
/*!40000 ALTER TABLE `css_responsive` DISABLE KEYS */;
/*!40000 ALTER TABLE `css_responsive` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.css_style_guide
CREATE TABLE IF NOT EXISTS `css_style_guide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_component` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(10) NOT NULL DEFAULT '0',
  `class` varchar(50) NOT NULL DEFAULT '0',
  `code` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.css_style_guide: ~5 rows (approximately)
DELETE FROM `css_style_guide`;
/*!40000 ALTER TABLE `css_style_guide` DISABLE KEYS */;
INSERT INTO `css_style_guide` (`id`, `type_component`, `tag`, `class`, `code`, `created_at`, `updated_at`) VALUES
	(1, 2, 'h1', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(2, 2, 'h2', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(3, 2, 'h3', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(4, 2, 'h4', '_font_arial', 'font-family: arial !important;', NULL, NULL),
	(5, 3, 'p', '_font_arial', 'font-family: arial !important;', NULL, NULL);
/*!40000 ALTER TABLE `css_style_guide` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.grids
CREATE TABLE IF NOT EXISTS `grids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `length` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `sections` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.grids: ~201 rows (approximately)
DELETE FROM `grids`;
/*!40000 ALTER TABLE `grids` DISABLE KEYS */;
INSERT INTO `grids` (`id`, `html_id`, `html_class`, `reference_id`, `length`, `sequence`, `sections`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, NULL, 12, 1, 1, NULL, NULL),
	(2, NULL, '_grid-badge-whatsapp', NULL, 1, 1, 1, NULL, NULL),
	(3, NULL, ' slider-preview tab-active', NULL, 12, 4, 2, NULL, NULL),
	(8, NULL, NULL, NULL, 8, 1, 3, NULL, NULL),
	(9, NULL, NULL, NULL, 4, 2, 3, NULL, NULL),
	(10, NULL, NULL, NULL, 12, 1, 4, NULL, NULL),
	(11, NULL, NULL, NULL, 3, 2, 4, NULL, NULL),
	(12, NULL, NULL, NULL, 3, 3, 4, NULL, NULL),
	(13, NULL, NULL, NULL, 3, 4, 4, NULL, NULL),
	(14, NULL, NULL, NULL, 3, 5, 4, NULL, NULL),
	(15, NULL, NULL, NULL, 12, 1, 2, NULL, NULL),
	(16, NULL, NULL, NULL, 6, 2, 2, NULL, NULL),
	(17, NULL, NULL, NULL, 6, 3, 2, NULL, NULL),
	(18, NULL, NULL, NULL, 12, 1, 5, NULL, NULL),
	(19, NULL, NULL, NULL, 12, 2, 5, NULL, NULL),
	(20, NULL, NULL, NULL, 12, 3, 5, NULL, NULL),
	(21, NULL, NULL, NULL, 4, 4, 5, NULL, NULL),
	(23, NULL, ' slider-preview', NULL, 12, 5, 2, NULL, NULL),
	(24, NULL, NULL, NULL, 4, 1, 6, NULL, NULL),
	(25, NULL, NULL, NULL, 4, 2, 6, NULL, NULL),
	(26, NULL, NULL, NULL, 4, 3, 6, NULL, NULL),
	(27, NULL, NULL, NULL, 4, 4, 6, NULL, NULL),
	(28, NULL, NULL, NULL, 4, 5, 6, NULL, NULL),
	(29, NULL, NULL, NULL, 4, 6, 6, NULL, NULL),
	(30, NULL, NULL, NULL, 6, 1, 7, NULL, NULL),
	(31, NULL, NULL, NULL, 6, 2, 7, NULL, NULL),
	(32, NULL, NULL, NULL, 12, 1, 8, NULL, NULL),
	(33, NULL, NULL, NULL, 6, 2, 8, NULL, NULL),
	(34, NULL, NULL, NULL, 6, 3, 8, NULL, NULL),
	(35, NULL, NULL, NULL, 12, 4, 8, NULL, NULL),
	(36, NULL, NULL, NULL, 12, 1, 9, NULL, NULL),
	(37, NULL, '_typografi', NULL, 12, 2, 9, NULL, NULL),
	(38, NULL, '_buttons', NULL, 12, 3, 9, NULL, NULL),
	(39, NULL, '_colors', NULL, 12, 4, 9, NULL, NULL),
	(40, NULL, '_icons', NULL, 12, 5, 9, NULL, NULL),
	(41, NULL, '_cards', NULL, 12, 6, 9, NULL, NULL),
	(42, NULL, '_image-videos', NULL, 12, 7, 9, NULL, NULL),
	(43, NULL, '_sliders', NULL, 12, 8, 9, NULL, NULL),
	(44, NULL, NULL, NULL, 12, 1, 10, NULL, NULL),
	(45, NULL, NULL, NULL, 8, 1, 11, NULL, NULL),
	(46, NULL, NULL, NULL, 4, 2, 11, NULL, NULL),
	(47, NULL, NULL, NULL, 5, 3, 11, NULL, NULL),
	(48, NULL, NULL, NULL, 7, 4, 11, NULL, NULL),
	(49, NULL, NULL, NULL, 12, 5, 11, NULL, NULL),
	(50, NULL, NULL, NULL, 6, 1, 12, NULL, NULL),
	(51, NULL, NULL, NULL, 6, 2, 12, NULL, NULL),
	(52, NULL, NULL, NULL, 6, 1, 13, NULL, NULL),
	(53, NULL, NULL, NULL, 6, 2, 13, NULL, NULL),
	(54, NULL, '_text_center _buttons', NULL, 12, 1, 14, NULL, NULL),
	(55, NULL, NULL, NULL, 4, 1, 14, NULL, NULL),
	(56, NULL, NULL, NULL, 4, 2, 14, NULL, NULL),
	(57, NULL, NULL, NULL, 4, 3, 14, NULL, NULL),
	(58, NULL, NULL, NULL, 6, 1, 15, NULL, NULL),
	(59, NULL, NULL, NULL, 6, 2, 15, NULL, NULL),
	(60, NULL, '_no_padding _text_center _buttons', NULL, 12, 3, 15, NULL, NULL),
	(61, NULL, '_no_padding', NULL, 6, 4, 15, NULL, NULL),
	(62, NULL, '_no_padding', NULL, 6, 5, 15, NULL, NULL),
	(63, NULL, '_no_padding', NULL, 6, 5, 15, NULL, NULL),
	(64, NULL, NULL, NULL, 12, 1, 16, NULL, NULL),
	(65, NULL, NULL, NULL, 12, 2, 16, NULL, NULL),
	(66, NULL, NULL, NULL, 12, 1, 17, NULL, NULL),
	(67, NULL, NULL, NULL, 12, 1, 18, NULL, NULL),
	(68, NULL, NULL, NULL, 8, 1, 19, NULL, NULL),
	(69, NULL, NULL, NULL, 4, 2, 19, NULL, NULL),
	(70, NULL, NULL, NULL, 7, 3, 19, NULL, NULL),
	(71, NULL, NULL, NULL, 5, 4, 19, NULL, NULL),
	(72, NULL, NULL, NULL, 12, 5, 19, NULL, NULL),
	(73, NULL, '_text_center _buttons', NULL, 12, 1, 20, NULL, NULL),
	(74, NULL, NULL, NULL, 12, 2, 20, NULL, NULL),
	(75, NULL, NULL, NULL, 12, 3, 20, NULL, NULL),
	(76, NULL, NULL, NULL, 12, 4, 20, NULL, NULL),
	(77, NULL, NULL, NULL, 12, 1, 21, NULL, NULL),
	(78, NULL, NULL, NULL, 12, 2, 21, NULL, NULL),
	(79, NULL, NULL, NULL, 12, 3, 21, NULL, NULL),
	(80, NULL, NULL, NULL, 12, 4, 21, NULL, NULL),
	(81, NULL, NULL, NULL, 12, 5, 21, NULL, NULL),
	(82, NULL, NULL, NULL, 12, 1, 22, NULL, NULL),
	(83, NULL, NULL, NULL, 7, 2, 23, NULL, NULL),
	(84, NULL, NULL, NULL, 5, 3, 23, NULL, NULL),
	(85, NULL, NULL, NULL, 6, 4, 23, NULL, NULL),
	(86, NULL, NULL, NULL, 6, 5, 23, NULL, NULL),
	(87, NULL, NULL, NULL, 7, 6, 23, NULL, NULL),
	(88, NULL, NULL, NULL, 5, 7, 23, NULL, NULL),
	(89, NULL, NULL, NULL, 5, 8, 24, NULL, NULL),
	(90, NULL, NULL, NULL, 7, 9, 24, NULL, NULL),
	(91, NULL, NULL, NULL, 6, 10, 24, NULL, NULL),
	(92, NULL, NULL, NULL, 6, 11, 24, NULL, NULL),
	(93, NULL, NULL, NULL, 6, 13, 24, NULL, NULL),
	(94, NULL, NULL, NULL, 6, 14, 24, NULL, NULL),
	(95, NULL, NULL, NULL, 12, 1, 25, NULL, NULL),
	(96, NULL, NULL, NULL, 12, 2, 25, NULL, NULL),
	(97, NULL, NULL, NULL, 12, 3, 25, NULL, NULL),
	(98, NULL, NULL, NULL, 12, 4, 25, NULL, NULL),
	(99, NULL, NULL, NULL, 7, 5, 25, NULL, NULL),
	(100, NULL, NULL, NULL, 5, 6, 25, NULL, NULL),
	(101, NULL, NULL, NULL, 12, 7, 25, NULL, NULL),
	(102, NULL, NULL, NULL, 5, 1, 26, NULL, NULL),
	(103, NULL, NULL, NULL, 7, 2, 26, NULL, NULL),
	(104, NULL, NULL, NULL, 7, 3, 26, NULL, NULL),
	(105, NULL, NULL, NULL, 5, 4, 26, NULL, NULL),
	(106, NULL, NULL, NULL, 12, 5, 26, NULL, NULL),
	(107, NULL, NULL, NULL, 12, 6, 26, NULL, NULL),
	(108, NULL, NULL, NULL, 5, 7, 26, NULL, NULL),
	(109, NULL, NULL, NULL, 7, 8, 26, NULL, NULL),
	(110, NULL, NULL, NULL, 7, 9, 26, NULL, NULL),
	(111, NULL, NULL, NULL, 5, 10, 26, NULL, NULL),
	(112, NULL, NULL, NULL, 5, 11, 26, NULL, NULL),
	(113, NULL, NULL, NULL, 7, 12, 26, NULL, NULL),
	(114, NULL, NULL, NULL, 5, 1, 27, NULL, NULL),
	(115, NULL, NULL, NULL, 7, 2, 27, NULL, NULL),
	(116, NULL, NULL, NULL, 7, 4, 27, NULL, NULL),
	(117, NULL, NULL, NULL, 5, 5, 27, NULL, NULL),
	(118, NULL, NULL, NULL, 5, 6, 27, NULL, NULL),
	(119, NULL, NULL, NULL, 7, 7, 27, NULL, NULL),
	(120, NULL, NULL, NULL, 8, 8, 27, NULL, NULL),
	(121, NULL, NULL, NULL, 12, 9, 27, NULL, NULL),
	(122, NULL, NULL, NULL, 7, 10, 27, NULL, NULL),
	(123, NULL, NULL, NULL, 5, 11, 27, NULL, NULL),
	(124, NULL, NULL, NULL, 7, 12, 27, NULL, NULL),
	(125, NULL, NULL, NULL, 5, 13, 27, NULL, NULL),
	(126, NULL, NULL, NULL, 6, 14, 27, NULL, NULL),
	(127, NULL, NULL, NULL, 6, 15, 27, NULL, NULL),
	(128, NULL, NULL, NULL, 7, 1, 28, NULL, NULL),
	(129, NULL, NULL, NULL, 5, 2, 28, NULL, NULL),
	(130, NULL, NULL, NULL, 5, 4, 13, NULL, NULL),
	(131, NULL, NULL, NULL, 7, 5, 13, NULL, NULL),
	(132, NULL, NULL, NULL, 12, 3, 13, NULL, NULL),
	(133, NULL, NULL, NULL, 12, 6, 13, NULL, NULL),
	(134, NULL, NULL, NULL, 6, 7, 13, NULL, NULL),
	(135, NULL, NULL, NULL, 6, 8, 13, NULL, NULL),
	(136, NULL, NULL, NULL, 12, 9, 13, NULL, NULL),
	(137, NULL, NULL, NULL, 6, 10, 13, NULL, NULL),
	(138, NULL, NULL, NULL, 6, 11, 13, NULL, NULL),
	(139, NULL, NULL, NULL, 12, 12, 13, NULL, NULL),
	(140, NULL, '', NULL, 12, 3, 16, NULL, NULL),
	(141, NULL, NULL, NULL, 12, 12, 24, NULL, NULL),
	(142, NULL, NULL, NULL, 12, 3, 27, NULL, NULL),
	(143, NULL, '_navbar-object', NULL, 12, 1, 29, NULL, NULL),
	(144, NULL, '_navbar-expand _main-list', NULL, 3, 2, 29, NULL, NULL),
	(145, NULL, '_navbar-expand _child-list', NULL, 3, 3, 29, NULL, NULL),
	(146, NULL, '_navbar-expand _preview-list', NULL, 6, 4, 29, NULL, NULL),
	(147, NULL, NULL, NULL, 12, 1, 33, NULL, NULL),
	(148, NULL, '_text_center', NULL, 12, 2, 33, NULL, NULL),
	(149, NULL, NULL, NULL, 12, 1, 34, NULL, NULL),
	(150, NULL, '_text_center', NULL, 3, 2, 34, NULL, NULL),
	(151, NULL, NULL, NULL, 9, 3, 34, NULL, NULL),
	(152, NULL, '_text_center', NULL, 3, 4, 34, NULL, NULL),
	(153, NULL, NULL, NULL, 9, 5, 34, NULL, NULL),
	(154, NULL, '_text_center', NULL, 3, 6, 34, NULL, NULL),
	(155, NULL, NULL, NULL, 9, 7, 34, NULL, NULL),
	(156, NULL, NULL, NULL, 12, 1, 38, NULL, NULL),
	(157, NULL, '_text_center', NULL, 12, 2, 38, NULL, NULL),
	(158, NULL, NULL, NULL, 12, 1, 39, NULL, NULL),
	(159, NULL, NULL, NULL, 6, 2, 39, NULL, NULL),
	(160, NULL, NULL, NULL, 6, 3, 39, NULL, NULL),
	(161, NULL, NULL, NULL, 12, 4, 39, NULL, NULL),
	(162, NULL, NULL, NULL, 6, 5, 39, NULL, NULL),
	(163, NULL, NULL, NULL, 6, 6, 39, NULL, NULL),
	(164, NULL, NULL, NULL, 12, 7, 39, NULL, NULL),
	(165, NULL, NULL, NULL, 6, 8, 39, NULL, NULL),
	(166, NULL, NULL, NULL, 6, 9, 39, NULL, NULL),
	(167, NULL, NULL, NULL, 12, 10, 39, NULL, NULL),
	(168, NULL, NULL, NULL, 6, 11, 39, NULL, NULL),
	(169, NULL, NULL, NULL, 6, 12, 39, NULL, NULL),
	(170, NULL, NULL, NULL, 12, 1, 41, NULL, NULL),
	(171, NULL, NULL, NULL, 12, 1, 42, NULL, NULL),
	(172, NULL, NULL, NULL, 12, 2, 42, NULL, NULL),
	(173, NULL, NULL, NULL, 4, 3, 42, NULL, NULL),
	(174, NULL, NULL, 173, 4, 4, 42, NULL, NULL),
	(175, NULL, NULL, 173, 4, 5, 42, NULL, NULL),
	(176, NULL, NULL, 173, 4, 6, 42, NULL, NULL),
	(177, NULL, NULL, 173, 4, 7, 42, NULL, NULL),
	(178, NULL, NULL, 173, 4, 8, 42, NULL, NULL),
	(179, NULL, NULL, 173, 4, 9, 42, NULL, NULL),
	(180, NULL, NULL, 173, 4, 10, 42, NULL, NULL),
	(181, NULL, NULL, 173, 4, 11, 42, NULL, NULL),
	(182, NULL, NULL, NULL, 12, 2, 41, NULL, NULL),
	(183, NULL, NULL, NULL, 12, 1, 44, NULL, NULL),
	(184, NULL, NULL, NULL, 12, 2, 44, NULL, NULL),
	(185, NULL, NULL, NULL, 12, 1, 45, NULL, NULL),
	(186, NULL, NULL, NULL, 4, 2, 46, NULL, NULL),
	(187, NULL, NULL, 186, 4, 3, 46, NULL, NULL),
	(188, NULL, NULL, 186, 4, 4, 46, NULL, NULL),
	(189, NULL, NULL, NULL, 12, 1, 46, NULL, NULL),
	(190, NULL, NULL, NULL, 12, 1, 48, NULL, NULL),
	(191, NULL, NULL, NULL, 12, 2, 48, NULL, NULL),
	(192, NULL, NULL, NULL, 12, 1, 49, NULL, NULL),
	(193, NULL, NULL, NULL, 6, 2, 49, NULL, NULL),
	(194, NULL, NULL, 193, 6, 3, 49, NULL, NULL),
	(195, NULL, NULL, 193, 6, 4, 49, NULL, NULL),
	(196, NULL, NULL, 193, 6, 5, 49, NULL, NULL),
	(197, NULL, NULL, 193, 6, 6, 49, NULL, NULL),
	(198, NULL, NULL, 193, 6, 7, 49, NULL, NULL),
	(199, NULL, NULL, NULL, 12, 1, 51, NULL, NULL),
	(200, NULL, NULL, NULL, 12, 2, 51, NULL, NULL),
	(201, NULL, NULL, NULL, 12, 1, 52, NULL, NULL),
	(202, NULL, NULL, NULL, 4, 2, 52, NULL, NULL),
	(203, NULL, NULL, 202, 4, 3, 52, NULL, NULL),
	(204, NULL, NULL, 202, 4, 4, 52, NULL, NULL),
	(205, NULL, NULL, 202, 4, 5, 52, NULL, NULL),
	(206, NULL, NULL, 202, 4, 6, 52, NULL, NULL);
/*!40000 ALTER TABLE `grids` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.groups: ~3 rows (approximately)
DELETE FROM `groups`;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'test group 1 updated', '2019-09-24 08:44:41', '2019-09-24 08:46:28'),
	(4, 'test', '2019-10-02 08:40:36', '2019-10-02 08:40:36'),
	(5, 'test', '2019-10-02 08:49:50', '2019-10-02 08:49:50');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.library_components
CREATE TABLE IF NOT EXISTS `library_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `css` text,
  `javascript` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table peugeot_indonesia.library_components: ~19 rows (approximately)
DELETE FROM `library_components`;
/*!40000 ALTER TABLE `library_components` DISABLE KEYS */;
INSERT INTO `library_components` (`id`, `name`, `css`, `javascript`, `created_at`, `updated_at`) VALUES
	(1, 'bootstrap_carousel_type_1', '.bootstrap_carousel_type_1 .carousel .carousel-control-next{\r\nposition: absolute;\r\nbottom: 10px;\r\nright: unset;\r\nleft: 38%;\r\nborder: 2px solid #2679BF;\r\npadding: 10px;\r\nwidth: 33px;\r\nheight: 33px;\r\nborder-radius: 15pc;\r\ntop: unset;\r\nbackground-color: transparent;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-next .carousel-control-next-icon\r\n{\r\n  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3E%3Cpath d=\'M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z\'/%3E%3C/svg%3E")!important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-prev .carousel-control-prev-icon\r\n{\r\n  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3E%3Cpath d=\'M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z\'/%3E%3C/svg%3E");!important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-control-prev{\r\nposition: absolute;\r\nbottom: 10px;\r\nright: 65%;\r\nborder: 2px solid #2679BF;\r\npadding: 10px;\r\nwidth: 33px;\r\nheight: 33px;\r\nborder-radius: 15pc;\r\ntop: unset;\r\nleft: unset;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-caption{\r\nvisibility:hidden;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-indicators{\r\nvisibility : hidden;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item{\r\npadding-left: 15%;\r\nheight: 250px;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item img{\r\n    right: 0;\r\n    width: 50% !important;\r\n}\r\n.bootstrap_carousel_type_1 .carousel .carousel-item .carousel-caption.d-none.d-md-block h5 {\r\n    visibility: visible;\r\n    position: absolute;\r\n    top: -180;\r\n    left: -60;\r\n    color: black;\r\n}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'bootstrap_carousel_type_2', '.bootstrap_carousel_type_2 .carousel .carousel-indicators {\r\n    position: absolute;\r\n    right: 25;\r\n    bottom: 35%;\r\n    top: 35%;\r\n    z-index: 15;\r\n    justify-content: center;\r\n    list-style: none;\r\n    float: unset;\r\n    left: unset;\r\n    display: unset;\r\n    padding: unset;\r\n    margin: unset;\r\n}\r\n\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators li {\r\n    box-sizing: content-box;\r\n    width: 10px;\r\n    height: 10px;\r\n    cursor: pointer;\r\n    background-color: #fff;\r\n    background-clip: padding-box;\r\n    margin: 15px;\r\n    border-radius: 15px;\r\n    flex: unset;\r\n    text-indent: unset;\r\n    border: unset;\r\n    transition: unset;\r\n    opacity: unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators {\r\n    visibility: visible;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-prev {\r\nposition: absolute;\r\n    bottom: 130px;\r\n    right: 60px;\r\n    border: 2px solid #2679BF;\r\n    padding: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 15pc;\r\n    top: unset;\r\n    left: unset;\r\n    opacity:unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-next {\r\n    background-color: transparent;\r\nposition: absolute;\r\n    bottom: 130px;\r\n    right: 15px;\r\n    border: 2px solid #2679BF;\r\n    padding: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 15pc;\r\n    top: unset;\r\n    opacity:unset;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-prev .carousel-control-prev-icon {\r\n    background-image: url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z\'/%3e%3c/svg%3e") !important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-control-next .carousel-control-next-icon {\r\n    background-image: url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%232679BF\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z\'/%3e%3c/svg%3e") !important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators{\r\nopacity:1!important;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators .active{\r\nopacity:1!important;\r\nbackground-color:#2679BF;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption{\r\nposition: absolute;\r\n    right: unset;\r\n    bottom: 0;\r\n    left: unset;\r\n    z-index: 10;\r\n    padding-top: 20px;\r\n    color: #fff;\r\n    text-align: center;\r\n    background-color: rgba(0,0,0,0.5);\r\n    width: 100%;\r\n    height: 120px;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption h5{\r\ndisplay:none;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-inner .carousel-item .carousel-caption p{\r\ntext-align: left;\r\n    padding: 20px;\r\n    margin-top: -20px;\r\n    font-size: 13px;\r\n}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'bootstrap_carousel_type_3', '.bootstrap_carousel_type_3 .carousel .carousel-indicators{\r\nvisibility:visible;\r\nbottom:20%;\r\nopacity:1!important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-indicators li{\r\nwidth: 8px;\r\n    height: 8px;\r\n    border: unset;\r\n    border-radius: 15pc;\r\n}\r\n.bootstrap_carousel_type_2 .carousel .carousel-indicators .active{\r\nopacity:1!important;\r\nbackground-color:#2679BF;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-next:hover{\r\nopacity:1 !important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-prev:hover{\r\nopacity:1 !important;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-next{\r\nright: unset;\r\n    border: unset;\r\n    padding: unset;\r\n    height: unset;\r\n    border-radius: unset;\r\n    background-color: transparent;\r\nposition: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n    -ms-flex-align: center;\r\n    align-items: center;\r\n    -webkit-box-pack: center;\r\n    -ms-flex-pack: center;\r\n    justify-content: center;\r\n    width: 15%;\r\n    color: #fff;\r\n    text-align: center;\r\n    opacity: .5;\r\nborder: unset;\r\n    padding: unset;\r\n    height: 50px;\r\n    width: 50px;\r\n    left: 93%;\r\n    right: unset;\r\n    border: 2px solid;\r\n    border-radius: 15pc;\r\n    margin: auto 10px;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-control-prev{\r\nright: unset;\r\n    border: unset;\r\n    padding: unset;\r\n    height: unset;\r\n    border-radius: unset;\r\n    background-color: transparent;\r\nposition: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n    -ms-flex-align: center;\r\n    align-items: center;\r\n    -webkit-box-pack: center;\r\n    -ms-flex-pack: center;\r\n    justify-content: center;\r\n    width: 15%;\r\n    color: #fff;\r\n    text-align: center;\r\n    opacity: .5;\r\nborder: unset;\r\n    padding: unset;\r\n    height: 50px;\r\n    width: 50px;\r\n    left: unset;\r\n    right: unset;\r\n    border: 2px solid;\r\n    border-radius: 15pc;\r\n    margin: auto 10px;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption {\r\nposition: absolute;\r\n    right: unset;\r\n    bottom: 0;\r\n    left: unset;\r\n    z-index: 10;\r\n    padding-top: 20px;\r\n    color: #fff;\r\n    text-align: center;\r\n    background-color: rgba(0,0,0,0.5);\r\n    width: 100%;\r\n    height: 120px;\r\n}\r\n\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption h5{\r\ndisplay:none;\r\n}\r\n.bootstrap_carousel_type_3 .carousel .carousel-inner .carousel-item .carousel-caption p{\r\ntext-align: left;\r\n    padding: 20px;\r\n    margin-top: -20px;\r\n    font-size: 13px;\r\n}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'slider-1', '.slider-1{\r\nheight: 650px;\r\n}\r\n.slider-1 .slider-item-container {\r\ndisplay: table;\r\nwidth:100%;\r\n}\r\n.slider-1 .slider-item-container .slider-item{\r\n    display: table-cell;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(1) {\r\n    top: 150px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(2) {\r\n    top:100px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(3) {\r\n    top:50px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(4) {\r\n    top:25px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(5) {\r\n    top:50px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(6) {\r\n    top:100px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item:nth-child(7) {\r\n    top:150px;\r\n    text-align: center;\r\n    height: 150px;\r\n    position: relative;\r\n    width: 320px;\r\n}\r\n.slider-1 .slider-item-container .slider-item img {\r\n    width: 80px;\r\n    height: 80px;\r\n    object-fit: cover;\r\n    object-position: center;\r\n    border-radius: 200px;\r\n    margin: 0px auto;\r\n    display: block;\r\n    cursor: pointer;\r\n    position: relative;\r\n    border: 1px solid black;\r\n    border-radius: 15pc;\r\n    padding: 20px;\r\n}\r\n.slider-1 .slider-item-container .slider-item .slider-heading{\r\n    position: relative;\r\n    top: 40%;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    text-align: center;\r\n    color: white;\r\n    display : none;\r\n}\r\n.slider-1 .slider-item-container .slider-item .slider-description{\r\n    position: relative;\r\n    padding-left: 35%;\r\n    color: white;\r\n    top: 35%;\r\n    display : none;\r\n}\r\n.slider-1 .preview-img {\r\n    width: 100%;\r\n    height: 400px;\r\n    margin-bottom:40px;\r\n\r\n}\r\n.slider-1 .preview-img img{\r\n    width: 30%;\r\n    object-fit: cover;\r\n    margin-left: 35%;\r\n    margin-right: 40%;\r\n    margin-top: 20px;\r\n}\r\n.slider-1 .preview-img .caption{\r\nmargin-top:80px;\r\n}\r\n.slider-1 .preview-img .caption h5{\r\n    visibility: visible;\r\n    color: black;\r\n    position: absolute;\r\n}\r\n.slider-1 .preview-img .caption p{\r\n    color: black;\r\n    position: absolute;\r\n    margin-top: 30px;\r\n    width: 100%;\r\n}', '<script type="text/javascript">\r\nvar elitem = $(\'.slider-1 .slider-item-container .slider-item\');\r\nvar elActive = $(\'.slider-1 .slider-item-container .active\');\r\nvar img = elActive.find(\'img\').attr(\'src\');\r\nvar heading = elActive.find(\'.slider-heading\').text();\r\nvar description = elActive.find(\'.slider-description\').text();\r\nconsole.log(elitem);\r\nconsole.log(heading);\r\nconsole.log(description);\r\nvar previewImg = \'<div class="preview-img"><img src="\' + img + \'"><div class="caption"><h5 class="slider-heading">\'+heading+\'</h5><p>\'+description+\'</p></div></div>\';\r\n$(previewImg).insertAfter($(\'.slider-1 .slider-item-container\'));\r\nelitem.click(function(){\r\n$(\'.preview-img img\').attr(\'src\', $(this).children(\'img\').attr(\'src\'));\r\nvar index =  $(this).children(\'h5\').text();\r\nvar indux =  $(this).children(\'p\').text();\r\nconsole.log(indux);\r\n$(\'.preview-img h5\').text(index);\r\n$(\'.preview-img p\').text(indux);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'slider-2', '.slider-2 .slider-item-container {\r\ndisplay: table;\r\nwidth:100%;\r\n}\r\n.slider-2 .slider-item-container .slider-item {\r\n    display: table-cell;\r\n    height: 200px;\r\n    position: relative;\r\n    cursor: pointer;\r\n    width: 320px;\r\n}\r\n.slider-2 .slider-item-container .slider-item img {\r\n    width: 100%;\r\n    height: 200px;\r\n    position: absolute;\r\n    object-fit: cover;\r\n    object-position: center;\r\n    background-color: #fff;\r\n}\r\n.slider-2 .slider-item-container .slider-item .slider-heading{\r\n    position: relative;\r\n    top: 40%;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    text-align: center;\r\n    color: black;\r\n}\r\n.slider-2 .slider-item-container .slider-item .slider-description{\r\nposition: relative;\r\n    color: black;\r\n    top: 35%;\r\n    text-align: center;\r\n}\r\n.slider-2 .slider-item-container .active .slider-heading{\r\n    color: white !important;\r\n}\r\n.slider-2 .slider-item-container .active .slider-description{\r\n    color: white !important;\r\n}\r\n.slider-2 .preview-img {\r\n    width: 100%;\r\n    margin-bottom:40px;\r\n}\r\n.slider-2 .preview-img img{\r\n    width: 100%;\r\n    height: 500px;\r\n    object-fit: cover;\r\n    object-position: center;\r\n}\r\n\r\n.slider-2 .preview-img h5{\r\n  top: 0;\r\n    margin: 200px 0px 0px 30px;\r\n    font-family: \'Permanent Marker\', cursive;\r\n    color: white;\r\n    position: absolute;\r\n}\r\n\r\n.slider-2 .preview-img p{\r\n    margin: 230px 0px 0px 30px;\r\n    color: white;\r\n    top: 0;\r\n    position: absolute;\r\n}\r\n.slider-2 .slider-item-container {\r\n    display: table;\r\n    width: 100%;\r\n    position: absolute;\r\n    bottom: 50;\r\n}\r\n.slider-2 .slider-item-container .active {\r\n    bottom: 20px;\r\n}\r\n.slider-2 .slider-item-container .slider-item::after {\r\n        content: \'\';\r\n    position: absolute;\r\n    left: 0;\r\n    bottom: -1px;\r\n    right: 0;\r\n    height: 30%;\r\n    background: linear-gradient(to bottom,rgba(255,255,255,0),#ccc) repeat left top;\r\n}\r\n.slider-2 .slider-item-container .active::after {\r\n        content: \'\';\r\n    position: absolute;\r\n    left: 0;\r\n    bottom: -1px;\r\n    right: 0;\r\n    height: 30%;\r\n    background: linear-gradient(to bottom,rgba(255,255,255,0),#113945) repeat left top;\r\n}', '<script type="text/javascript">\r\nvar elitem = $(\'.slider-2 .slider-item-container .slider-item\');\r\nvar elActive = $(\'.slider-2 .slider-item-container .active\');\r\nvar img = elActive.find(\'img\').attr(\'src\');\r\nvar heading = elActive.find(\'.slider-heading\').text();\r\nvar description = elActive.find(\'.slider-description\').text();\r\nconsole.log(elitem);\r\nconsole.log(heading);\r\nconsole.log(description);\r\nvar previewImg = \'<div class="preview-img"><img src="\' + img + \'"><h5 class="slider-heading">\'+heading+\'</h5><p>\'+description+\'</p></div>\';\r\n$(previewImg).insertBefore($(\'.slider-2 .slider-item-container\'));\r\nelitem.click(function(){\r\nelitem.removeClass(\'active\');\r\n$(this).addClass(\'active\');\r\n$(\'.preview-img img\').attr(\'src\', $(this).children(\'img\').attr(\'src\'));\r\nvar index =  $(this).children(\'h5\').text();\r\nvar indux =  $(this).children(\'p\').text();\r\nconsole.log(indux);\r\n$(\'.preview-img h5\').text(index);\r\n$(\'.preview-img p\').text(indux);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 'slider-3', '.slider-3{\r\nmargin-top:20px;\r\n}\r\n\r\n.slider-3 .slider-item-container {\r\ndisplay: table;    \r\nwidth: 75%;\r\n    margin: 0px auto;\r\n}\r\n\r\n.slider-item{\r\ndisplay: table-cell;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item img{\r\nwidth: 126px;\r\n    height: 126px;\r\n    border-radius: 15pc;\r\ncursor:pointer;\r\ndisplay:block;\r\nmargin:0px auto;\r\nborder: 1px solid;\r\nbackground-color:#fff;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item img:first-child, .slider-3 .slider-item-container .slider-item img:last-child{\r\nz-index:6;\r\nleft:40%;\r\nright:40%;\r\n}\r\n.slider-3 .slider-item-container .slider-item img:nth-child(2), .slider-3 .slider-item-container .slider-item img:nth-child(4){\r\nz-index:9;\r\n}\r\n.slider-3 .slider-item-container .slider-item:first-child{\r\nwidth: 130px;\r\n    height: 130px;\r\n    left: 20%;\r\n    position: absolute;\r\n}\r\n.slider-3 .slider-item-container .slider-item:nth-child(2){\r\nposition: absolute;\r\n    left: 30%;\r\n    width: 130px;\r\n    height: 130px;\r\n}\r\n.slider-3 .slider-item-container .slider-item:nth-child(4){\r\n    position: absolute;\r\n    right: 30%;\r\n    width: 130px;\r\n    height: 130px;\r\nz-index:9;\r\n}\r\n.slider-3 .slider-item-container .slider-item:last-child{\r\nwidth: 130px;\r\n    height: 130px;\r\n    right: 20%;\r\n    position: absolute;\r\nz-index:6;\r\n}\r\n.slider-3 .slider-item-container .active img{\r\nwidth: 200px;\r\n    height: 200px;\r\ndisplay:block;\r\nmargin:0px auto;\r\nposition: absolute;\r\n    top: 0;\r\nz-index:10!important;\r\n}\r\n\r\n.slider-3 .slider-item-container .active h5 {\r\n    visibility: visible!important;\r\ntext-align: center;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item h5{\r\nvisibility:hidden;\r\nmargin-top: 200px;\r\n}\r\n\r\n.slider-3 .slider-item-container .slider-item p{\r\nvisibility:hidden!important;\r\n}', '<script type="text/javascript">\r\nvar switchActive = $(\'.slider-3 .slider-item-container .slider-item:nth-child(3)\');\r\nvar itemActive = $(\'.slider-3 .slider-item-container .active\');\r\nitemActive.removeClass(\'active\');\r\nswitchActive.addClass(\'active\');\r\nvar itemClick = $(\'.slider-3 .slider-item-container .slider-item\');\r\nitemClick.click(function(){\r\nvar Active = $(\'.slider-3 .slider-item-container .active\');\r\nvar src = $(this).find(\'img\');\r\nvar image = src.attr(\'src\');\r\nvar srcActive = Active.find(\'img\');\r\nvar imageActive = srcActive.attr(\'src\');\r\nvar heading = $(this).find(\'h5\').text();\r\nvar headingActive = Active.find(\'h5\').text();\r\nconsole.log(heading);\r\nconsole.log(headingActive);\r\nsrc.attr(\'src\', imageActive);\r\nsrcActive.attr(\'src\', image);\r\nsrc.attr(\'src\', imageActive);\r\n$(this).find(\'h5\').html(headingActive);\r\nActive.find(\'h5\').html(heading);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 'slider-4', 'h2{\r\n   font-size:35px;\r\n   position: absolute;\r\n    top: 5px;\r\n    left: 41%;\r\n}\r\n.slider-4 .slider-item-container .slider-item h2 {\r\n    display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active h2 {\r\n    display: block;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active {\r\nwidth: 40%;\r\n    height: 465px;\r\n    position: inherit;\r\n    object-fit: cover;\r\n    cursor:unset;\r\n}\r\n.slider-4 .slider-item-container .slider-item{\r\n    width: 18%;\r\n    height: 100px;\r\n    float: left;\r\n    display: table-cell;\r\n    position: relative;\r\n    top: 365px;\r\n    margin: 0px 10px 0px 0px;\r\n    cursor:pointer;\r\n}\r\n.slider-4 .slider-item-container .slider-item img {\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slider-4 .slider-item-container .slider-item p {\r\n    position: absolute;\r\n    top: 20%;\r\n    left: 41%;\r\n   display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active p {\r\n    position: absolute;\r\n    top: 20%;\r\n    left: 41%;\r\n    display: block !important;\r\n   font-size:20px;\r\n}\r\n.slider-4 .slider-item-container .slider-item h5 {\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: none;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active h5 {\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: block !Important;\r\n   font-size:20px;\r\n}\r\n.slider-4 .slider-item-container .slider-item.active img{\r\n    width: 100%;\r\n    height: 100%;\r\n    object-fit: cover;\r\n}\r\n.slider-4 .slider-item-container .slider-item:before {\r\n  content: \'\';\r\n  display: block;\r\n  position:absolute;\r\n  width:100%;\r\n  transition: height 0.5s ease-out;\r\n  background:rgba(0,0,0,0.3);\r\n}\r\n.slider-4 .slider-item-container .slider-item:hover:before {\r\n  height: 100%;\r\n}\r\n.slider-4 .slider-item-container .active:before {\r\n  content: \'\';\r\n  display: none;\r\n  position:absolute;\r\n  width:100%;\r\n  transition: height 0.5s ease-out;\r\n  background:rgba(0,0,0,0.3);\r\n}\r\n.slider-4 .slider-item-container .active:hover:before {\r\n  height: 100%;\r\n}\r\n.slider-4 .slider-item-container .slider-item:hover .slider-heading{\r\ndisplay: block !important;\r\nopacity:1 !important;\r\n    color: white;\r\n    left: unset;\r\n    top: 0;\r\n    width: 100%;\r\n    text-align: center;\r\n    margin-top: 40px;\r\n    font-size: 16px;\r\n}\r\n.slider-4 .slider-item-container .active:hover .slider-heading{\r\nopacity:1 !important;\r\n    color: black;\r\n    left: unset;\r\n    top: unset;\r\n    width: unset;\r\n    text-align: unset;\r\n    margin-top: unset;\r\n    font-size: unset;\r\n    position: absolute;\r\n    top: 10%;\r\n    left: 41%;\r\n   display: block !Important;\r\n   font-size:20px;\r\n}', '<script type="text/javascript">\r\n$(\'<h2 class="_font-permanent-marker">Konservasi Penyu</h2>\').insertBefore(\'.slider-4 .slider-item-container .slider-item h5\')\r\nvar switchActive = $(\'.slider-4 .slider-item-container .slider-item:first-child\');\r\nvar itemActive = $(\'.slider-4 .slider-item-container .active\');\r\nitemActive.removeClass(\'active\');\r\nswitchActive.addClass(\'active\');\r\nvar itemClick = $(\'.slider-4 .slider-item-container .slider-item\');\r\nitemClick.click(function(){\r\nvar Active = $(\'.slider-4 .slider-item-container .active\');\r\nvar src = $(this).find(\'img\');\r\nvar image = src.attr(\'src\');\r\nvar srcActive = Active.find(\'img\');\r\nvar imageActive = srcActive.attr(\'src\');\r\nvar heading = $(this).find(\'h5\').text();\r\nvar headingActive = Active.find(\'h5\').text();\r\nconsole.log(heading);\r\nconsole.log(headingActive);\r\nsrc.attr(\'src\', imageActive);\r\nsrcActive.attr(\'src\', image);\r\nsrc.attr(\'src\', imageActive);\r\n$(this).find(\'h5\').html(headingActive);\r\nActive.find(\'h5\').html(heading);\r\n});\r\n</script>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 'slider-5', '', '', NULL, NULL),
	(10, 'slider-6', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 'slider-7', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 'video-1', '.video-1{\r\n     position:relative;\r\n}\r\n.video-1 embed{\r\n     display:none;\r\n}\r\n.video-1 .video-cover {\r\n     max-width:100%;\r\n     height:auto;\r\n}\r\n.video-1 .video-button {\r\n     position:absolute;\r\n     z-index:666;\r\n     top:50%;\r\n     left:50%;\r\n     transform:translate(-50%, -50%);\r\n     background-color:transparent;\r\n     border:0;\r\n     width:40px;\r\n    cursor:pointer;\r\n}', '<script type="text/javascript">\r\nvar cover = \'<img class="video-cover" src="./uploads/image/coverVideo-cockpit.png">\';\r\nvar button = \'<img class="video-button" src="./uploads/image/btnVideo.png">\'\r\n$(\'._videoPlayer\').wrap(\'<div class="video-1"></div>\');\r\n$(\'.video-1\').append( cover+button);\r\n$(\'.video-1 .video-button\').on(\'click\', function(e) {\r\n	e.preventDefault();\r\n	$(".video-1 ._videoPlayer")[0].src += "?autoplay=1";\r\n	$(\'.video-1 ._videoPlayer\').show();\r\n	$(\'.video-1 .video-cover\').hide();\r\n	$(\'.video-1 .video-button\').hide();\r\n})\r\n</script>', NULL, NULL),
	(13, 'accordion-1', '.accordion-1{\r\n     width: 100%;\r\n}\r\n.accordion-1 .accordion .card .card-header .mb-0 button{\r\n     width: 100%;\r\n    border: unset;\r\n    background: #172A4F;\r\n    color: white;\r\n    line-height: 27px;\r\n    cursor: pointer;\r\n    padding: 20px;\r\n}\r\n.accordion-1 .accordion .card .card-header .mb-0{\r\n    margin:0px;\r\n}\r\n.accordion-1 .accordion .card .card-header .mb-0 button .fa-plus{\r\n     border: 1px solid white;\r\n    color: white;\r\n    padding: 3px 4px 1px 4px;\r\n    border-radius: 15pc;\r\n    margin-bottom: 2.5px;\r\n}\r\n.accordion-1 .accordion .card .collapse .card-body{\r\n    display: none;\r\n    top: -200px;\r\n    position: relative;\r\n}\r\n.accordion-1 .accordion .card .collapse .card-body.active{\r\n    display:block;\r\n    top: 0;\r\n    transition: display,top 1s ease-in;\r\n}\r\n\r\n@media (max-width: 1200px) and (min-width: 993px) {\r\n.accordion-1{\r\n     max-width: 1200px;\r\n     min-width: 993px;\r\n}\r\n}\r\n\r\n@media (max-width: 992px) and (min-width: 769px) {\r\n.accordion-1{\r\n     max-width: 992px;\r\n     min-width: 769px;\r\n}\r\n}\r\n\r\n@media (max-width: 768px) and (min-width: 426px) {\r\n.accordion-1{\r\n     max-width: 768px;\r\n     min-width: 426px;\r\n}\r\n}\r\n\r\n@media (max-width: 425px) and (min-width: 321px){\r\n.accordion-1{\r\n     max-width: 425px;\r\n     min-width: 321px;\r\n}\r\n}\r\n\r\n@media (max-width: 320px) and (min-width: 0px) {\r\n.accordion-1{\r\n     max-width: 320px;\r\n     min-width: 200px;\r\n}\r\n}', '<script type="text/javascript">\r\n$(\'.accordion\').wrap(\'<div class="accordion-1"></div>\')\r\n$(\'.accordion-1 .accordion .card .card-header .mb-0 .btn\').click(function(){\r\n    if($(\'.accordion-1 .accordion .card .collapse .card-body.active\').length == 0){\r\n	$(\'.accordion-1 .accordion .card .collapse .card-body\').addClass(\'active\');\r\n    }else{\r\n	$(\'.accordion-1 .accordion .card .collapse .card-body\').removeClass(\'active\');\r\n    }\r\n})\r\n</script>', NULL, NULL),
	(14, 'slider-8', '', '', NULL, NULL),
	(15, 'slider-9', '', '\r\n', NULL, NULL),
	(16, 'slider-10', NULL, '', NULL, NULL),
	(17, 'slider-11', NULL, NULL, NULL, NULL),
	(18, 'card-1', '.card-1{\r\n\r\n}\r\n.card-1 img{\r\n    object-fit: cover;\r\n    max-width: 370px;\r\n    max-height: 255px;\r\n}\r\n.card-1 ._card-text-container{\r\n    max-width: 370px;\r\n}\r\n.card-1 ._card-text-container ._card-heading {\r\n    margin: 22.5px 0px 29px;\r\n    font-size: 24px;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    line-height: 29px;\r\n}\r\n.card-1 ._card-text-container ._card-text:nth-child(2) {\r\n    font-style: normal;\r\n    font-weight: 300;\r\n    font-size: 14px;\r\n    line-height: 135%;\r\n    margin: 0;\r\n}\r\n.card-1 ._card-text-container p._card-text:nth-child(3) {\r\n    margin-top: 41.5px;\r\n    margin-bottom: 0;\r\n}', NULL, NULL, NULL),
	(19, 'card-2', '.card-2 {\r\n    position: relative;\r\n    display: block;\r\n    height: auto;\r\n    overflow: hidden;\r\n}\r\n.card-2 img{\r\n    object-fit: cover;\r\n    width: 100%;\r\n    position: relative;\r\n}\r\n.card-2 ._card-text-container{\r\n    padding: 69px;\r\n    color:white;\r\n    position: absolute;\r\n    top: 0px;\r\n    z-index: 1;\r\n    height: 100%;\r\n    box-sizing: border-box;\r\n    width: 80%;\r\n}\r\n.card-2 ._card-text-container ._card-heading {\r\n    margin: 0px 0px 20px;\r\n    font-size: 31px;\r\n    font-weight: bolder;\r\n    line-height: 29px;\r\n}\r\n.card-2 ._card-text-container ._card-text:nth-child(2) {\r\n    font-weight: 300;\r\n    font-size: 16px;\r\n    line-height: 24px;\r\n}\r\n.card-2 ._card-text-container p._card-text:nth-child(3) {\r\n    display:none;\r\n}\r\n\r\n', NULL, NULL, NULL),
	(20, 'card-3', '.card-3 {\r\n    width: 80%;\r\n    margin: 0px auto;\r\n}\r\n.card-3 img{\r\n    object-fit: cover;\r\n    width: 100%;\r\n    max-height: 255px;\r\n}\r\n.card-3 ._card-text-container{\r\n    position: relative;\r\n    padding: 0px;\r\n}\r\n.card-3 ._card-text-container ._card-heading {\r\n    margin: 0px 0px 10px;\r\n    font-size: 16px !important;\r\n    line-height: 29px;\r\n}\r\n.card-3 ._card-text-container ._card-text:nth-child(2) {\r\n    font-weight: 300;\r\n    font-size: 14px;\r\n    line-height: 24px;\r\n}\r\n.card-3 ._card-text-container p._card-text:nth-child(3) {\r\n    display:none;\r\n}', NULL, NULL, NULL);
/*!40000 ALTER TABLE `library_components` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.migrations: ~14 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_09_11_054233_create_group_table', 1),
	(4, '2019_09_11_054242_create_page_table', 1),
	(5, '2019_09_11_054304_create_public_table', 1),
	(6, '2019_09_11_054311_create_grid_table', 1),
	(7, '2019_09_11_054319_create_component_table', 1),
	(8, '2019_09_11_054340_create_library_component_table', 1),
	(9, '2019_09_11_054348_create_library_grid_table', 1),
	(10, '2019_09_11_054353_create_library_css_table', 1),
	(11, '2019_09_11_054359_create_library_javascript_table', 1),
	(12, '2019_09_23_094038_create_project_table', 1),
	(13, '2019_09_23_094217_create_section_table', 1),
	(14, '2019_09_23_123936_create_type_component_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `features_images` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_index` tinyint(4) NOT NULL DEFAULT '0',
  `group` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `published_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.pages: ~11 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name`, `features_images`, `description`, `is_index`, `group`, `parent`, `status`, `published_at`, `created_at`, `updated_at`) VALUES
	(1, 'home', NULL, NULL, 1, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(2, 'styleguide', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(3, 'product', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(4, 'new 5008', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(5, 'new 3008', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(6, 'contact us', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(7, 'after sales service', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(8, 'news', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(9, 'detail', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(10, 'service', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL),
	(11, 'network', NULL, NULL, 0, NULL, NULL, 1, '0000-00-00 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.projects
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.projects: ~1 rows (approximately)
DELETE FROM `projects`;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'peugeot indonesia', 1, NULL, NULL);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.publics
CREATE TABLE IF NOT EXISTS `publics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `parent` int(11) DEFAULT NULL,
  `pages` int(11) DEFAULT NULL,
  `projects` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.publics: ~11 rows (approximately)
DELETE FROM `publics`;
/*!40000 ALTER TABLE `publics` DISABLE KEYS */;
INSERT INTO `publics` (`id`, `name`, `url`, `meta`, `parent`, `pages`, `projects`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'home', 'home', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">', NULL, 1, 1, 1, NULL, NULL),
	(2, 'styleguide', 'styleguide', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 2, 1, 1, NULL, NULL),
	(3, 'product', 'product', NULL, NULL, NULL, NULL, 1, NULL, NULL),
	(4, 'new 5008', 'new-5008', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', 3, 4, 1, 1, NULL, NULL),
	(5, 'new 3008', 'new-3008', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', 3, 5, 1, 1, NULL, NULL),
	(6, 'contact us', 'contact-us', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 6, 1, 1, NULL, NULL),
	(7, 'after sales service', 'after-sales-service', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 7, 1, 1, NULL, NULL),
	(8, 'news', 'news', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 8, 1, 1, NULL, NULL),
	(9, 'detail', 'detail', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', 8, 9, 1, 1, NULL, NULL),
	(10, 'service', 'service', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 10, 1, 1, NULL, NULL),
	(11, 'network', 'network', '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>\r\n<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">\r\n<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>', NULL, 11, 1, 1, NULL, NULL);
/*!40000 ALTER TABLE `publics` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.sections
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `pages` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.sections: ~52 rows (approximately)
DELETE FROM `sections`;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` (`id`, `name`, `html_id`, `html_class`, `reference_id`, `sequence`, `pages`, `created_at`, `updated_at`) VALUES
	(1, 'section main banner', NULL, '_section-main-banner _container-full', NULL, 1, 1, NULL, NULL),
	(2, 'section preview product', NULL, '_section-preview-product _container-full', NULL, 2, 1, NULL, NULL),
	(3, 'section product video', NULL, '_section-product-video', NULL, 3, 0, NULL, NULL),
	(4, 'section offer', NULL, '_section-offer', NULL, 4, 1, NULL, NULL),
	(5, 'section body paint', NULL, '_section-body-paint', NULL, 5, 1, NULL, NULL),
	(6, 'section dealer', NULL, '_section-dealer', NULL, 6, 1, NULL, NULL),
	(7, 'section exibition', NULL, '_section-exibition _container-full', NULL, 7, 1, NULL, NULL),
	(8, 'section event promotion', NULL, '_section-event-promotion _container-full', NULL, 8, 1, NULL, NULL),
	(9, 'section style guide', NULL, '_section-style-guide', NULL, 1, 2, NULL, NULL),
	(10, 'section main banner', NULL, '_section _section-main-banner _container-full', NULL, 1, 4, NULL, NULL),
	(11, 'section advantage', NULL, '_section-advantage', NULL, 2, 4, NULL, NULL),
	(12, 'section design', NULL, '_section-design', NULL, 3, 4, NULL, NULL),
	(13, 'section design interior', NULL, '_section-design-interior _sg_bg_color_3', NULL, 6, 4, NULL, NULL),
	(14, 'section design modular', NULL, '_section-design-modular _section-style-guide', NULL, 5, 4, NULL, NULL),
	(15, 'section design exterior', NULL, '_section-design-exterior _sg_bg_color_3', NULL, 4, 4, NULL, NULL),
	(16, 'section performance', NULL, '_section-performance', NULL, 7, 4, NULL, NULL),
	(17, 'section bottom banner', NULL, '_section-bottom-banner', NULL, 8, 4, NULL, NULL),
	(18, 'section main banner', NULL, '_section-main-banner', NULL, 1, 5, NULL, NULL),
	(19, 'section advantage', NULL, '_section-advantage', NULL, 2, 5, NULL, NULL),
	(20, 'section award', NULL, '_section-award _sg_bg_color_3', NULL, 3, 5, NULL, NULL),
	(21, 'section dimension', NULL, '_section-dimension _sg_bg_color_3', NULL, 4, 5, NULL, NULL),
	(22, 'section modular design', NULL, '_section-modular-design', NULL, 5, 5, NULL, NULL),
	(23, 'section overall design', NULL, '_section-overall-design _sg_bg_color_3', NULL, 6, 5, NULL, NULL),
	(24, 'section modular preview', NULL, '_section-modular-preview _sg_bg_color_3', NULL, 7, 5, NULL, NULL),
	(25, 'section cockpit', NULL, '_section-cockpit', NULL, 8, 5, NULL, NULL),
	(26, 'section additional feature', NULL, '_section-additional-feature _sg_bg_color_3', NULL, 9, 5, NULL, NULL),
	(27, 'section safety feature', NULL, '_section-safety-feature', NULL, 10, 5, NULL, NULL),
	(28, 'section bottom offer', NULL, '_section-bottom-offer', NULL, 11, 5, NULL, NULL),
	(29, 'section navbar', NULL, '_section-navbar', NULL, 2, 2, NULL, NULL),
	(30, 'section navbar', NULL, '_section-navbar', 29, 0, 1, NULL, NULL),
	(31, 'section navbar', NULL, '_section-navbar', 29, 0, 4, NULL, NULL),
	(32, 'section navbar', NULL, '_section-navbar', 29, 0, 5, NULL, NULL),
	(33, 'section main banner', NULL, '_section-main-banner', NULL, 1, 6, NULL, NULL),
	(34, 'section contact detail', NULL, '_section-contact-detail', NULL, 2, 6, NULL, NULL),
	(35, 'section dealer', NULL, '_section-dealer', 6, 3, 6, NULL, NULL),
	(36, 'section navbar', NULL, '_section-navbar', 29, 0, 6, NULL, NULL),
	(37, 'section navbar', NULL, '_section-navbar', 29, 0, 7, NULL, NULL),
	(38, 'section main banner', NULL, '_section-main-banner', NULL, 1, 7, NULL, NULL),
	(39, 'section after sales detail', NULL, '_section-after-sales-detail', NULL, 2, 7, NULL, NULL),
	(40, 'section navbar', NULL, '_section-navbar', 29, 0, 8, NULL, NULL),
	(41, 'section main banner', NULL, '_section-main-banner', NULL, 1, 8, NULL, NULL),
	(42, 'section news list', NULL, '_section-news-list', NULL, 2, 8, NULL, NULL),
	(43, 'section navbar', NULL, '_section-navbar', 29, 0, 9, NULL, NULL),
	(44, 'section main banner', NULL, '_section-main-banner', NULL, 1, 9, NULL, NULL),
	(45, 'section news detail', NULL, '_section-news-detail', NULL, 2, 9, NULL, NULL),
	(46, 'section news list', NULL, '_section-news-list', NULL, 3, 9, NULL, NULL),
	(47, 'section navbar', NULL, '_section-navbar', 29, 0, 10, NULL, NULL),
	(48, 'section main banner', NULL, '_section-main-banner', NULL, 1, 10, NULL, NULL),
	(49, 'section service detail', NULL, '_section-service-detail', NULL, 2, 10, NULL, NULL),
	(50, 'section navbar', NULL, '_section-navbar', 29, 0, 11, NULL, NULL),
	(51, 'section main banner', NULL, '_section-main-banner', NULL, 1, 11, NULL, NULL),
	(52, 'section network detail', NULL, '_section-network-detail', NULL, 2, 11, NULL, NULL);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table peugeot_indonesia.type_component
CREATE TABLE IF NOT EXISTS `type_component` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table peugeot_indonesia.type_component: ~12 rows (approximately)
DELETE FROM `type_component`;
/*!40000 ALTER TABLE `type_component` DISABLE KEYS */;
INSERT INTO `type_component` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'banner', NULL, NULL),
	(2, 'heading', NULL, NULL),
	(3, 'paragraph', NULL, NULL),
	(4, 'image', NULL, NULL),
	(5, 'video', NULL, NULL),
	(6, 'slider', NULL, NULL),
	(7, 'button', NULL, NULL),
	(8, 'link', NULL, NULL),
	(9, 'icon', NULL, NULL),
	(10, 'accordion', NULL, NULL),
	(11, 'breadcrumb', NULL, NULL),
	(12, 'card', NULL, NULL);
/*!40000 ALTER TABLE `type_component` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
